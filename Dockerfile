# Step 1
FROM node:14.15 as builder  
RUN mkdir /app 
WORKDIR /app 
COPY package.json /app 
RUN yarn install 
COPY . /app 
ENV PORT=8080
EXPOSE 8080 
RUN yarn build 


# Stage 2
FROM nginx:latest 
COPY --from=builder /app/build /usr/share/nginx/html

# DEPLOYMENT 
# docker build -t gkaruga/crwn-clothing-react:1.0 .
# docker run -d -p 5000:8080 af3c6ca12df3 
# docker run -d -p 5000:80 -t gkaruga/crwn-clothing-react:1.0
# docker container rm 889dc70ef0c5 cf90bf459da3   

# PUSH TO DOCKER HUB 
# docker push gkaruga/crwn-clothing-react:1.0 
