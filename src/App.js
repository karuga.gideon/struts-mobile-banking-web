import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// Apps 
import About from "./components/pages/app/About";
import HomePage from "./components/pages/HomePage";
import Dashboard from "./components/pages/app/Dashboard";
import Loans from "./components/pages/app/Loans";
import LoanTypes from "./components/pages/app/LoanTypes";
import Members from "./components/pages/app/Members";
import Transactions from "./components/pages/app/Transactions";
import Users from "./components/pages/app/Users";
import Reports from "./components/pages/app/Reports";
import Profile from "./components/pages/app/Profile";
import SignIn from "./components/pages/SignIn";
import SignUp from "./components/pages/SignUp";
import SysMsgs from "./components/pages/app/SysMsgs";
import USSDLogs from "./components/pages/app/USSDLogs";


// Forms
import CreateLoanType from "./components/pages/forms/create_loan_type";
import ActivateMember from "./components/pages/forms/activate_member";
import ChangePassword from "./components/pages/forms/ChangePassword";
import CreateTransaction from "./components/pages/forms/create_transaction";
import CreateUser from "./components/pages/forms/create_user";

// View
import LoanDetails from "./components/pages/view/LoanDetails";
import LoanTypeDetails from "./components/pages/view/LoanTypeDetails";
import MemberDetails from "./components/pages/view/MemberDetails";
import SysMsgDetails from "./components/pages/view/SysMsgDetails";
import TransactionDetails from "./components/pages/view/TransactionDetails";
import UserDetails from "./components/pages/view/UserDetails";
import USSDLogDetails from "./components/pages/view/USSDLogDetails";


class App extends React.Component {
  render() {
    return (
      <div>
        <Router className="App">
          <React.Fragment>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route path="/signin" component={SignIn} />
              <Route path="/signup" component={SignUp} />

              {/* Apps */}
              <Route path="/about" component={About} />
              <Route path="/dashboard" component={Dashboard} />
              <Route path="/loans" component={Loans} />
              <Route path="/loan_types" component={LoanTypes} />
              <Route path="/members" component={Members} />
              <Route path="/transactions" component={Transactions} />
              <Route path="/reports" component={Reports} />
              <Route path="/users" component={Users} />
              <Route path="/profile" component={Profile} />
              <Route path="/change-password" component={ChangePassword} />
              <Route path="/ussd_logs" component={USSDLogs} />
              <Route path="/sys_msgs" component={SysMsgs} />


              {/* Forms  */}
              <Route path="/activate-member" component={ActivateMember} />
              <Route path="/create-loan-type" component={CreateLoanType} />
              <Route path="/create-transaction" component={CreateTransaction} />
              <Route path="/create-user" component={CreateUser} />

              {/* Views */}              
              <Route path="/loan/:loanID" component={LoanDetails} />
              <Route path="/loan-type/:loanTypeID" component={LoanTypeDetails} />
              <Route path="/member/:memberID" component={MemberDetails} />              
              <Route path="/sys_msg/:sysMsgID" component={SysMsgDetails} />
              <Route path="/transaction/:transactionID" component={TransactionDetails} />
              <Route path="/user/:userID" component={UserDetails} />
              <Route path="/ussd_log/:ussdLogID" component={USSDLogDetails} />
              

              {/* <Route path="*" component={NotFoundPage} /> */}
              <Route path="*" component={HomePage} />
            </Switch>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App;

// Admin theme downloaded from
// https://demos.creative-tim.com/paper-dashboard-react/#/dashboard
