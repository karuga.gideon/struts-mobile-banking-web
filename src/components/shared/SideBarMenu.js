import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import { Link } from "react-router-dom";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  toolbar: theme.mixins.toolbar,
}));

export default function ClippedDrawer() {

  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  function handleClick() {
    setOpen(!open);
  }


  return (
    <div style={{ marginTop: 80 }}>
      <CssBaseline />              
         <List>
            <ListItem button component={Link} to="/dashboard">
                <ListItemIcon>
                <SendIcon />
                </ListItemIcon>
                <ListItemText primary="Dashboard" />
            </ListItem>
            <ListItem button component={Link} to="/drafts">
                <ListItemIcon>
                <DraftsIcon />
                </ListItemIcon>
                <ListItemText primary="Drafts" />
            </ListItem>
            <ListItem button onClick={handleClick}>
                <ListItemIcon>
                <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="Inbox" />
                 {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
                <Collapse in={open} timeout="auto" unmountOnExit style={{marginLeft: "10px"}}>
                    <List component="div" disablePadding>
                        <ListItem button className={classes.nested} component={Link} to="/starred">
                            <ListItemIcon>
                            <StarBorder />
                            </ListItemIcon>
                            <ListItemText primary="Starred" />
                        </ListItem>
                        <ListItem button className={classes.nested} component={Link} to="/fav">
                            <ListItemIcon>
                            <StarBorder />
                            </ListItemIcon>
                            <ListItemText primary="Favourites" />
                        </ListItem>
                    </List>
                </Collapse>
         </List>
            
        <Divider />
        <List>
           <ListItem button component={Link} to="/allmail">
                <ListItemIcon>
                <MailIcon />
                </ListItemIcon>
                <ListItemText primary="All Mail" />
            </ListItem>
            <ListItem button component={Link} to="/trash">
                <ListItemIcon>
                <DraftsIcon />
                </ListItemIcon>
                <ListItemText primary="Trash" />
            </ListItem>
            <ListItem button component={Link} to="/spam">
                <ListItemIcon>
                <DraftsIcon />
                </ListItemIcon>
                <ListItemText primary="Spam" />
            </ListItem>
        </List>      

    </div>
  );
}
