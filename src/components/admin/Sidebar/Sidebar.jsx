import React from "react";
import { NavLink } from "react-router-dom";
import { Nav } from "reactstrap";


class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.activeRoute.bind(this);
  }
  // verifies if routeName is the one active (in browser input)
  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
  }

  render() {
    return (
      <div
        className="sidebar"
        data-color={this.props.bgColor}
        data-active-color={this.props.activeColor}
      >                
          <Nav className="sidebar-nav">
          <ul className="nav">
            <li className={this.activeRoute("dashboard")}>
              <NavLink
                to="/dashboard"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-bank" />
                <p>Dashboard</p>
              </NavLink>
            </li>                
            <li className={
              this.activeRoute("collections") || 
              this.activeRoute("create-collection") || 
              this.activeRoute("collection-details") }>
              <NavLink
                to="/collections"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-tile-56" />
                <p>Collections</p>
              </NavLink>
            </li>
            <li
              className={
                this.activeRoute("clients") ||
                this.activeRoute("create-client") ||
                this.activeRoute("client-details") ||
                this.activeRoute("client-questions")
              }
            >
              <NavLink
                to="/clients"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-bullet-list-67" />
                <p>Clients</p>
              </NavLink>
            </li>      
            <li className={
                  this.activeRoute("sales_reps") || 
                  this.activeRoute("create-sales-rep") || 
                  this.activeRoute("sales-rep")
                }>
              <NavLink
                to="/sales_reps"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-tile-56" />
                <p>Sales Reps</p>
              </NavLink>
            </li>  
            <li className={this.activeRoute("reports")}>
              <NavLink
                to="/reports"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-tile-56" />
                <p>Reports</p>
              </NavLink>
            </li>
            <li className={ 
                  this.activeRoute("users") || 
                  this.activeRoute("create-user") ||
                  this.activeRoute("user-details")
                }>
              <NavLink
                to="/users"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-tile-56" />
                <p>Users</p>
              </NavLink>
            </li>
            <li className={
                this.activeRoute("profile") || 
                this.activeRoute("change-password")
                }>
              <NavLink
                to="/profile"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-badge" />
                <p>Profile</p>
              </NavLink>
            </li>
            <li className={this.activeRoute("about")}>
              <NavLink
                to="/about"
                className="nav-link"
                activeClassName="active"
              >
                <i className="nc-icon nc-diamond" />
                <p>About</p>
              </NavLink>
            </li>       
            </ul>     
          </Nav>
        </div>
    );
  }
}

export default Sidebar;
