import React from "react";
import { Card, CardHeader, CardBody, CardTitle, Row, Col } from "reactstrap";

import { Form, Label } from "semantic-ui-react";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import { getUserDetails, getAccessToken } from "../../../util/AuthService";

class ActivateMember extends React.Component {
    constructor(props) {
        super(props);
        document.title = "Activate Member";
        this.state = {
            phoneNumber: ""
        };
    }

    handleActiveClick = color => {
        this.setState({ activeColor: color });
    };

    handleBgClick = color => {
        this.setState({ backgroundColor: color });
    };

    componentDidMount() {

        // this.checkAuth(); 
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        // Retrieve User Types... 
        console.log("Retrieving user types....?");

        axios({
            url: API_BASE_URL + "/usertypes",
            method: "GET",
            withCredentials: false
        })
            .then(response => {
                var json_data = JSON.parse(JSON.stringify(response.data));
                console.log("Printing usertypes...");

                this.setState({ userTypes: json_data.user_types });
            })
            .catch(err => {
                console.log(err);
            });

    }

    onSubmit = e => {

        this.setState({ isLoading: true });

        var clientPayLoad = {
            phone_number: this.state.phoneNumber,
        };

        var loggedIn = getUserDetails();


        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        console.log("Validating member...");
        console.log(clientPayLoad);

        axios({
            url: `${API_BASE_URL}/members/validate_cbs`,
            method: "POST",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            },
            data: clientPayLoad
        })
            .then(response => {
                this.setState({ isLoading: false });

                if (!response.data.error) {
                    this.props.history.push("/members");
                } else {
                    alert(response.data.details);
                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    };

    render() {

        let activateMemberButton = this.state.isLoading ? (
            <Loader />
        ) : (
                <Form.Field
                    primary
                    className="field btn btn-primary btn-xl text-uppercase"
                    type="submit"
                    color="green"
                    onClick={this.onSubmit}
                >
                    Check Number
                </Form.Field>
            );

        let backButton = this.state.isLoading ? (
            <Loader />
        ) : (
                <Link
                    to="/members"
                    className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                    style={{ backgroundColor: "rgb(107, 208, 152)" }}
                >
                    <Menu.Item name="Back" />
                </Link>
            );

        return (

            <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

                {loadHeader()}

                <div className="app-body">

                    {loadSidebarMenu()}

                    <main className="main">

                        <div
                            className="main-panel"
                            ref="mainPanel"
                            style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                        >


                            {/* <!-- Breadcrumb--> */}
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">Home</li>
                                <li className="breadcrumb-item">
                                    <a href="dashboard">Admin</a>
                                </li>
                                <li className="breadcrumb-item active">Members</li>
                                <li className="breadcrumb-item active">Activate Member</li>
                                {/* <!-- Breadcrumb Menu--> */}
                                <li className="breadcrumb-menu d-md-down-none">
                                    <div className="btn-group" role="group" aria-label="Button group">
                                        <a className="btn" href="dashboard">
                                            <i className="icon-speech"></i>
                                        </a>
                                        <a className="btn" href="dashboard">
                                            <i className="icon-graph"></i>  Dashboard</a>
                                        <a className="btn" href="clients">
                                            <i className="icon-settings"></i>  Settings</a>
                                    </div>
                                </li>
                            </ol>
                            <div className="container-fluid">

                                <Row>
                                    <Col md={8} xs={8}>
                                        <Card className="card-user">
                                            <CardHeader>
                                                <CardTitle>Activate Member</CardTitle>
                                            </CardHeader>
                                            <CardBody>
                                                <Form>

                                                    {/* Phone Number  */}
                                                    <div className="col-lg-12">
                                                        <Label
                                                            style={{
                                                                fontSize: "0.8571em",
                                                                marginBottom: "5px",
                                                                color: "#9A9A9A"
                                                            }}
                                                        >
                                                            Phone Number
                        </Label>
                                                        <Form.Field className="form-control">
                                                            <input
                                                                type="text"
                                                                placeholder="Phone Number*"
                                                                className=""
                                                                style={{
                                                                    textDecoration: "none",
                                                                    padding: "0px",
                                                                    border: "0px"
                                                                }}
                                                                onChange={e =>
                                                                    this.setState({ phoneNumber: e.target.value })
                                                                }
                                                            />
                                                        </Form.Field>
                                                    </div>
                                                    <br />

                                                    <div className="col-lg-12">
                                                        {activateMemberButton} &nbsp; {backButton}
                                                        <br />
                                                        <br />
                                                    </div>
                                                </Form>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            </div>

                            <FixedPlugin
                                bgColor={this.state.backgroundColor}
                                activeColor={this.state.activeColor}
                                handleActiveClick={this.handleActiveClick}
                                handleBgClick={this.handleBgClick}
                            />
                        </div>
                    </main>
                </div>
            </div>
        );
    }
}

export default ActivateMember;
