import React from "react";
import { Card, CardHeader, CardBody, CardTitle, Row, Col } from "reactstrap";

import { Form, Label } from "semantic-ui-react";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import { getUserDetails, getAccessToken } from "../../../util/AuthService";

class CreateLoanType extends React.Component {
    constructor(props) {
        super(props);
        document.title = "Create Loan Type";
        this.state = {
            backgroundColor: "black",
            activeColor: "warning",
            loan_code: "",
            loan_name: "",
            min_loan_amount: 0,
            max_loan_amount: 0,
            installments: 0
        };
    }

    handleActiveClick = color => {
        this.setState({ activeColor: color });
    };

    handleBgClick = color => {
        this.setState({ backgroundColor: color });
    };

    componentDidMount() {

        // this.checkAuth(); 
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        // Retrieve User Types... 
        console.log("Retrieving user types....?");

        axios({
            url: API_BASE_URL + "/usertypes",
            method: "GET",
            withCredentials: false
        })
            .then(response => {
                var json_data = JSON.parse(JSON.stringify(response.data));
                console.log("Printing usertypes...");

                this.setState({ userTypes: json_data.user_types });
            })
            .catch(err => {
                console.log(err);
            });

    }


    onSubmit = e => {

        this.setState({ isLoading: true });

        var clientPayLoad = {
            loan_code: this.state.loan_code,
            loan_name: this.state.loan_name,
            min_loan_amount: parseInt(this.state.min_loan_amount, 10),
            max_loan_amount: parseInt(this.state.max_loan_amount, 10),
            installments: parseInt(this.state.installments, 10)
        };

        var loggedIn = getUserDetails();


        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        console.log("Creating User...");
        console.log(clientPayLoad);

        axios({
            url: `${API_BASE_URL}/loan_types`,
            method: "POST",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            },
            data: clientPayLoad
        })
            .then(response => {
                this.setState({ isLoading: false });

                if (!response.data.error) {
                    this.props.history.push("/loan_types");
                } else {
                    alert(response.data.details);
                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    };

    render() {

        let addLoanTypeButton = this.state.isLoading ? (
            <Loader />
        ) : (
                <Form.Field
                    primary
                    className="field btn btn-primary btn-xl text-uppercase"
                    type="submit"
                    color="green"
                    onClick={this.onSubmit}
                >
                    Add LoanType
                </Form.Field>
            );

        let backButton = this.state.isLoading ? (
            <Loader />
        ) : (
                <Link
                    to="/loan_types"
                    className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                    style={{ backgroundColor: "rgb(107, 208, 152)" }}
                >
                    <Menu.Item name="Back" />
                </Link>
            );

        return (

            <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

                {loadHeader()}

                <div className="app-body">

                    {loadSidebarMenu()}

                    <main className="main">

                        <div
                            className="main-panel"
                            ref="mainPanel"
                            style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                        >


                            {/* <!-- Breadcrumb--> */}
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">Home</li>
                                <li className="breadcrumb-item">
                                    <a href="dashboard">Admin</a>
                                </li>
                                <li className="breadcrumb-item active">LoanTypes</li>
                                <li className="breadcrumb-item active">Add LoanType</li>
                                {/* <!-- Breadcrumb Menu--> */}
                                <li className="breadcrumb-menu d-md-down-none">
                                    <div className="btn-group" role="group" aria-label="Button group">
                                        <a className="btn" href="dashboard">
                                            <i className="icon-speech"></i>
                                        </a>
                                        <a className="btn" href="dashboard">
                                            <i className="icon-graph"></i>  Dashboard</a>
                                        <a className="btn" href="clients">
                                            <i className="icon-settings"></i>  Settings</a>
                                    </div>
                                </li>
                            </ol>
                            <div className="container-fluid">

                                <Row>
                                    <Col md={8} xs={8}>
                                        <Card className="card-user">
                                            <CardHeader>
                                                <CardTitle>Add Loan Type</CardTitle>
                                            </CardHeader>
                                            <CardBody>
                                                <Form>

                                                    {/* Loan Code  */}
                                                    <div className="col-lg-12">
                                                        <Label
                                                            style={{
                                                                fontSize: "0.8571em",
                                                                marginBottom: "5px",
                                                                color: "#9A9A9A"
                                                            }}
                                                        >
                                                            Loan Code
                        </Label>
                                                        <Form.Field className="form-control">
                                                            <input
                                                                type="text"
                                                                placeholder="Loan Code*"
                                                                className=""
                                                                style={{
                                                                    textDecoration: "none",
                                                                    padding: "0px",
                                                                    border: "0px"
                                                                }}
                                                                onChange={e =>
                                                                    this.setState({ loan_code: e.target.value })
                                                                }
                                                            />
                                                        </Form.Field>
                                                    </div>
                                                    <br />

                                                    {/* Loan Name  */}
                                                    <div className="col-lg-12">
                                                        <Label
                                                            style={{
                                                                fontSize: "0.8571em",
                                                                marginBottom: "5px",
                                                                color: "#9A9A9A"
                                                            }}
                                                        >
                                                            Loan Name
                        </Label>
                                                        <Form.Field className="form-control">
                                                            <input
                                                                type="text"
                                                                placeholder="Loan Name*"
                                                                className=""
                                                                style={{
                                                                    textDecoration: "none",
                                                                    padding: "0px",
                                                                    border: "0px"
                                                                }}
                                                                onChange={e =>
                                                                    this.setState({ loan_name: e.target.value })
                                                                }
                                                            />
                                                        </Form.Field>
                                                    </div>
                                                    <br />

                                                    {/* Min Loan Amount  */}
                                                    <div className="col-lg-12">
                                                        <Label
                                                            style={{
                                                                fontSize: "0.8571em",
                                                                marginBottom: "5px",
                                                                color: "#9A9A9A"
                                                            }}
                                                        >
                                                            Min Loan Amount
                        </Label>
                                                        <Form.Field className="form-control">
                                                            <input
                                                                type="text"
                                                                pattern="[0-9]*"
                                                                placeholder="Min Loan Amount*"
                                                                className=""
                                                                style={{
                                                                    textDecoration: "none",
                                                                    padding: "0px",
                                                                    border: "0px"
                                                                }}
                                                                onChange={e =>
                                                                    this.setState({ min_loan_amount: e.target.value })
                                                                }
                                                            />
                                                        </Form.Field>
                                                    </div>
                                                    <br />


                                                    {/* Max Loan Amount  */}
                                                    <div className="col-lg-12">
                                                        <Label
                                                            style={{
                                                                fontSize: "0.8571em",
                                                                marginBottom: "5px",
                                                                color: "#9A9A9A"
                                                            }}
                                                        >
                                                            Max Loan Amount
                        </Label>
                                                        <Form.Field className="form-control">
                                                            {/* <label>Oranization Name</label> */}
                                                            <input
                                                                type="text"
                                                                pattern="[0-9]*"
                                                                placeholder="Max Loan Amount"
                                                                className=""
                                                                style={{
                                                                    textDecoration: "none",
                                                                    padding: "0px",
                                                                    border: "0px"
                                                                }}
                                                                onChange={e =>
                                                                    this.setState({ max_loan_amount: e.target.value })
                                                                }
                                                            />
                                                        </Form.Field>
                                                        <br />
                                                    </div>

                                                    {/* Installments  */}
                                                    <div className="col-lg-12">
                                                        <Label
                                                            style={{
                                                                fontSize: "0.8571em",
                                                                marginBottom: "5px",
                                                                color: "#9A9A9A"
                                                            }}
                                                        >
                                                            Installments
                        </Label>
                                                        <Form.Field className="form-control">
                                                            {/* <label>Oranization Name</label> */}
                                                            <input
                                                                type="text"
                                                                pattern="[0-9]*"
                                                                placeholder="Installments"
                                                                className=""
                                                                style={{
                                                                    textDecoration: "none",
                                                                    padding: "0px",
                                                                    border: "0px"
                                                                }}
                                                                onChange={e =>
                                                                    this.setState({ installments: e.target.value })
                                                                }
                                                            />
                                                        </Form.Field>
                                                        <br />
                                                    </div>


                                                    <div className="col-lg-12">
                                                        {addLoanTypeButton} &nbsp; {backButton}
                                                        <br />
                                                        <br />
                                                    </div>
                                                </Form>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                </Row>
                            </div>

                            <FixedPlugin
                                bgColor={this.state.backgroundColor}
                                activeColor={this.state.activeColor}
                                handleActiveClick={this.handleActiveClick}
                                handleBgClick={this.handleBgClick}
                            />
                        </div>
                    </main>
                </div>
            </div>
        );
    }
}

export default CreateLoanType;
