import React from "react";
import { Card, CardBody, CardHeader, CardTitle, Row, Col } from "reactstrap";
import Header from "../../admin/Header/Header.jsx";
import Sidebar from "../../admin/Sidebar/Sidebar.jsx";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import dashboardRoutes from "../../../routes/dashboard.jsx";
import { Link } from "react-router-dom";
import { Table } from "semantic-ui-react";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";

import { Form } from "semantic-ui-react";

import { getUserDetails } from "../../../util/AuthService";


class Surveys extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Survey";
    this.state = {
      backgroundColor: "black",
      activeColor: "warning",
      records: [],
      graphArray: [],
      isLoading: false,
      pageOfItems: []
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {

    var loggedIn = getUserDetails();


    var securityToken = "";

    if (loggedIn != null) {
      securityToken = getAccessToken();
    }


    axios.defaults.headers.common["x-mb-token"] = securityToken;

    console.log("Getting clients....?");

    axios({
      url: API_BASE_URL + "/surveys/organization/5",
      method: "GET",
      withCredentials: false
    })
      .then(response => {

        var json_data = JSON.parse(JSON.stringify(response.data));


        if (!response.data.error) {
          this.setState({ records: json_data });


          this.setState({ graphArray: json_data });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {

    const { records } = this.state;
    let viewButton = this.state.isLoading ? (
      <Loader />
    ) : (
        <Form.Field
          primary
          className="field btn btn-primary btn-xl text-uppercase"
          type="submit"
          color="green"
          style={{ backgroundColor: "#6bd098" }}
          onClick={this.onSubmit}
        >
          View
        </Form.Field>
      );


    let questionsButton = this.state.isLoading ? (
      <Loader />
    ) : (
        <Form.Field
          primary
          className="field btn btn-primary btn-xl text-uppercase"
          type="submit"
          color="green"
          style={{ backgroundColor: "#4acccd", marginLeft: "5px" }}
          onClick={this.onSubmit}
        >
          Questions
        </Form.Field>
      );

    // #fed136

    return (
      <div className="wrapper">
        <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        />
        <div className="main-panel" ref="mainPanel">
          <Header {...this.props} />
          <div className="content">
            <Row>
              <Col xs={12}>
                <Card>
                  <CardHeader>
                    <CardTitle tag="h4">My Surveys</CardTitle>
                    {/* <Link to="create-survey">
                      <Button color="primary" round>
                        Create Survey
                      </Button>
                    </Link> */}
                  </CardHeader>
                  <CardBody
                    style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                  >
                    <Table
                      celled
                      selectable
                      responsive
                      style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                    >
                      <Table.Header>
                        <Table.Row style={{ color: "#51cbce" }}>
                          <Table.HeaderCell>Title</Table.HeaderCell>
                          <Table.HeaderCell>Description</Table.HeaderCell>
                          <Table.HeaderCell>Date Created</Table.HeaderCell>
                          <Table.HeaderCell>status</Table.HeaderCell>
                          <Table.HeaderCell>Action</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>

                      <Table.Body>
                        {records.map(record => (
                          <Table.Row>
                            <Table.Cell>{record.title}</Table.Cell>
                            <Table.Cell>{record.description}</Table.Cell>
                            <Table.Cell>{record.created_at}</Table.Cell>
                            <Table.Cell>{record.status}</Table.Cell>
                            <Table.Cell>
                              <Link to={`/survey-details/${record.id}`}>
                                {viewButton}
                              </Link>

                              <Link to={`/survey-questions/${record.id}`}>
                                {questionsButton}
                              </Link>
                            </Table.Cell>
                          </Table.Row>
                        ))}
                      </Table.Body>
                    </Table>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        </div>
        <FixedPlugin
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
          handleActiveClick={this.handleActiveClick}
          handleBgClick={this.handleBgClick}
        />
      </div>
    );
  }
}

export default Surveys;
