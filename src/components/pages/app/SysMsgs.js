import React from "react";
import { Card, CardBody, CardHeader, CardTitle, Row, Col } from "reactstrap";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Table, Form } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { FaFileExcel, FaFileCsv } from 'react-icons/fa';

import { getUserDetails, isLoggedIn, getAccessToken } from "../../../util/AuthService";
import ReactPaginate from 'react-paginate';


class SysMsgs extends React.Component {
    constructor(props) {
        super(props);
        document.title = "SysMsgs";
        this.state = {
            backgroundColor: "black",
            activeColor: "warning",
            records: [],
            recordsSearch: [],
            recordsCount: 0,
            graphArray: [],
            isLoading: false,
            pageOfItems: [],
            page: 1,
            recordsPerPage: 10,
            data: [],
            offset: 0,
            per: 10,
            currentRecords: [],
            startDate: new Date(),
            endDate: new Date(),
            reportTypes: ["Export", "Excel", "CSV"],
            reportExportType: "Excel"
        };

        this.handleSearch = this.handleSearch.bind(this);
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
        this.handleDateFilter = this.handleDateFilter.bind(this);
        this.handleReportExport = this.handleReportExport.bind(this);
        this.handleClick = this.handleClick.bind(this);

        loadSidebarMenu.bind(this);
    }

    handleSearch(e) {

        var queryParameter = e.target.value;
        console.log("Searching by >>> " + queryParameter);

        if (queryParameter.length === 0) {
            queryParameter = "query"
        }

        this.checkAuth();
        var loggedIn = getUserDetails();


        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        axios({
            url: API_BASE_URL + "/sys_msgs",
            method: "GET",
            withCredentials: false
        })
            .then(response => {
                var json_data = JSON.parse(JSON.stringify(response.data));

                if (!response.data.error) {
                    this.setState({ records: json_data.sys_msgs });
                    this.setState({ data: json_data.sys_msgs });
                    this.setState({ currentRecords: json_data.sys_msgs });
                    this.setState({ recordsCount: json_data.pagination.count });
                    this.setState({ recordsSearch: json_data.sys_msgs });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }


    handleClick(event) {
        this.setState({
            page: Number(event.target.id)
        });
    }

    handleActiveClick = color => {
        this.setState({ activeColor: color });
    };

    handleBgClick = color => {
        this.setState({ backgroundColor: color });
    };

    handleStartDateChange(date) {
        this.setState({
            startDate: date
        });
    }

    handleEndDateChange(date) {
        this.setState({
            endDate: date
        });
    }

    handleReportExport(event) {

        var reportType = event;
        var loggedIn = getUserDetails();
        var startDate = Date.parse(this.state.startDate);
        var endDate = Date.parse(this.state.endDate);
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;
        axios({
            url: API_BASE_URL + "/export/sys_msgs/" + reportType + "/" + startDate + "/" + endDate,
            method: "GET",
            withCredentials: false
        })
            .then(response => {

                if (reportType === "excel") {
                    console.log("Downloading excel report...");
                    this.downloadURL("http://localhost:9010/downloads/Members_Excel.xlsx");
                }

                if (reportType === "csv") {
                    console.log("Downloading CSV Report...");
                    document.getElementById('download_csv').click();
                }

            })
            .catch(err => {
                console.log(err);
            });
    }

    downloadURL(url) {
        var hiddenIFrameID = 'hiddenDownloader',
            iframe = document.getElementById(hiddenIFrameID);
        if (iframe === null) {
            iframe = document.createElement('iframe');
            iframe.id = hiddenIFrameID;
            iframe.style.display = 'none';
            document.body.appendChild(iframe);
        }
        iframe.src = url;
    };


    handleDateFilter() {

        var loggedIn = getUserDetails();
        var startDate = Date.parse(this.state.startDate);
        var endDate = Date.parse(this.state.endDate);
        console.log("SysMsgs date filter report search...");
        console.log("startDate >>>  " + startDate);
        console.log("endDate   >>>> " + endDate);

        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }

        axios.defaults.headers.common["x-mb-token"] = securityToken;

        axios({
            url: API_BASE_URL + "/sys_msgs",
            method: "GET",
            withCredentials: false
        })
            .then(response => {

                var json_data = JSON.parse(JSON.stringify(response.data));


                if (!response.data.error) {
                    this.setState({ records: json_data.sys_msgs });
                    this.setState({ recordsCount: json_data.pagination.count });
                    this.setState({ recordsSearch: json_data.sys_msgs });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

    componentDidMount() {

        this.checkAuth();
        var loggedIn = getUserDetails();


        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        axios({
            url: API_BASE_URL + "/sys_msgs",
            method: "GET",
            withCredentials: true,
            headers: {
                'x-mb-token': securityToken
            }
        })
            .then(response => {

                var json_data = JSON.parse(JSON.stringify(response.data));

                if (!response.data.error) {
                    this.setState({ records: json_data.sys_msgs });
                    this.setState({ data: json_data.sys_msgs });
                    this.setState({ currentRecords: json_data.sys_msgs });
                    this.setState({ recordsCount: json_data.pagination.count });
                    this.setState({ recordsSearch: json_data.sys_msgs });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

    componentWillMount() {
        this.checkAuth();
    }

    checkAuth() {
        var loggedIn = isLoggedIn();
        if (!loggedIn) {
            this.props.history.push("/");
        } else {
            var userDetails = getUserDetails();
            this.setState({
                name: userDetails.name,
                phone: userDetails.msisdn,
                email: userDetails.email
            });
        }
    }

    handlePageClick = data => {
        this.loadSysMsgsFromServer(data);
    };


    loadSysMsgsFromServer(data) {

        var selectedPage = data.selected + 1; 
        const { per } = this.state;

        this.checkAuth();
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }

        axios.defaults.headers.common["x-mb-token"] = securityToken;
        axios({
            url: API_BASE_URL + "/sys_msgs?page="+selectedPage+"&per="+per,
            method: "GET",
            withCredentials: false
        })
            .then(response => {

                var json_data = JSON.parse(JSON.stringify(response.data));

                if (!response.data.error) {
                    this.setState({ records: json_data.sys_msgs });
                    this.setState({ data: json_data.sys_msgs });
                    this.setState({ currentRecords: json_data.sys_msgs });
                    this.setState({ recordsCount: json_data.pagination.count });
                    this.setState({ recordsSearch: json_data.sys_msgs });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }


    render() {

        const { currentRecords, recordsPerPage, recordsCount } = this.state;

        // Logic for displaying page numbers
        const recordsPageNumbers = [];
        var numberOfPages = Math.ceil(recordsCount / recordsPerPage); 

        for (let i = 1; i <= numberOfPages; i++) {
            recordsPageNumbers.push(i);
        }


        let searchBox = (

            <div style={{ float: "right", clear: "both" }}>

                <input style={{ marginRight: "10px", borderRadius: "5px", fontSize: "12pt", float: "right" }}
                    type="text" className="input" placeholder="Search..." onChange={this.handleSearch} />

            </div>);

        let createMemberButton = this.state.isLoading ? (
            <Loader />
        ) : (
                <div style={{}}>

                    &nbsp;&nbsp;&nbsp;

                    <div style={{ float: "right", marginRight: "0px", zIndex: "6", width: "55%" }}>

                        <Row>
                            <Col md={2} xs={2} style={{ marginRight: "35px" }}>
                                <DatePicker
                                    selected={this.state.startDate}
                                    onChange={this.handleStartDateChange}
                                    dateFormat="dd/MM/yyyy"
                                    // minDate= { subDays(new Date(), 5) }
                                    // maxDate= { new Date()}
                                    style={{ zIndex: "5" }}
                                />
                            </Col>

                            <Col md={2} xs={2} style={{ marginLeft: "45px" }}>
                                <DatePicker
                                    selected={this.state.endDate}
                                    onChange={this.handleEndDateChange}
                                    dateFormat="dd/MM/yyyy"
                                    // minDate= { subDays(new Date(), 5) }
                                    // maxDate= { new Date()}
                                    style={{ zIndex: "5" }}
                                />
                            </Col>

                            <Col md={1} xs={1} style={{ marginLeft: "75px" }}>
                                <Form.Field
                                    primary
                                    className="field btn btn-primary btn-sm text-uppercase"
                                    type="submit"
                                    color="blue"
                                    style={{ backgroundColor: "#20a8d8" }}
                                    onClick={this.handleDateFilter}
                                >
                                    Filter
            </Form.Field>
                            </Col>

                            <Col md={2} xs={2} style={{ marginLeft: "45px" }}>

                                <Form.Field
                                    primary
                                    className="field btn btn-primary btn-sm"
                                    // type="submit"
                                    color="blue"
                                    style={{ backgroundColor: "#20a8d8" }}
                                    // onClick={this.handleReportExport("excel")}
                                    onClick={() => this.handleReportExport("excel")}
                                >
                                    Excel <FaFileExcel />
                                    <span className="badge badge-primary"></span>
                                </Form.Field>
                            </Col>

                            <Col md={2} xs={2} style={{ marginLeft: "-45px" }}>
                                
                                <Form.Field
                                    primary
                                    className="field btn btn-primary btn-sm"
                                    type="submit"
                                    color="blue"
                                    style={{ backgroundColor: "#20a8d8" }}
                                    onClick={() => this.handleReportExport("csv")}
                                >
                                    CSV <FaFileCsv />
                                    <span className="badge badge-primary"></span>
                                </Form.Field>
                            </Col>                           

                        </Row>

                    </div>

                </div>
            );

        let viewButton = this.state.isLoading ? (
            <Loader />
        ) : (
                <Form.Field
                    // primary
                    className="field btn btn-primary btn-xl text-uppercase"
                    type="submit"
                    color="green"
                    style={{ backgroundColor: "#6bd098" }}
                    onClick={this.onSubmit}
                >
                    View
                </Form.Field>
            );


        return (
            <div className="">

                <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

                    {loadHeader()}

                    <div className="app-body">

                        {loadSidebarMenu("/sys_msgs")}

                        <main className="main">


                            <div className="main-panel" ref="mainPanel">

                                {/* <!-- Breadcrumb--> */}
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item">Home</li>
                                    <li className="breadcrumb-item">
                                        <Link to="dashboard">Admin</Link>
                                    </li>
                                    <li className="breadcrumb-item active">SysMsgs</li>
                                    {/* <!-- Breadcrumb Menu--> */}
                                    <li className="breadcrumb-menu d-md-down-none">
                                        <div className="btn-group" role="group" aria-label="Button group">
                                            <Link className="btn" to="dashboard">
                                                <i className="icon-speech"></i>
                                            </Link>
                                            <Link className="btn" to="dashboard">
                                                <i className="icon-graph"></i>  Dashboard</Link>
                                            <Link className="btn" to="sys_msgs">
                                                <i className="icon-settings"></i>  Settings</Link>
                                        </div>
                                    </li>
                                </ol>

                                <div className="container-fluid">

                                    <Row>
                                        <Col xs={12}>
                                            <Card style={{ height: "100%" }}>
                                                <CardHeader>
                                                    <CardTitle tag="h5">SysMsgs ({recordsCount}) &nbsp; {searchBox} </CardTitle>
                                                </CardHeader>
                                                <CardBody style={{ overflowX: "hidden", msOverflowY: "auto", height: "100%" }}>

                                                    {createMemberButton}

                                                    <a href="http://localhost:9010/downloads/Members_CSV.csv" download id="download_csv" hidden>Download</a>


                                                    <br />
                                                    <Table
                                                        celled
                                                        selectable
                                                        // responsive
                                                        style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                                                    >
                                                        <Table.Header>
                                                            <Table.Row style={{ color: "#51cbce" }}>
                                                                <Table.HeaderCell>ID</Table.HeaderCell>
                                                                <Table.HeaderCell>Message Type</Table.HeaderCell>
                                                                <Table.HeaderCell>Recipient</Table.HeaderCell>
                                                                <Table.HeaderCell>Message</Table.HeaderCell>
                                                                <Table.HeaderCell>Status</Table.HeaderCell>
                                                                <Table.HeaderCell>Date Created</Table.HeaderCell>
                                                                <Table.HeaderCell>Action</Table.HeaderCell>
                                                            </Table.Row>
                                                        </Table.Header>

                                                        <Table.Body>
                                                            {currentRecords.map(record => (
                                                                <Table.Row key={record.id}>
                                                                    <Table.Cell>{record.id}</Table.Cell>
                                                                    <Table.Cell>{record.message_type}</Table.Cell>
                                                                    <Table.Cell>{record.recipient}</Table.Cell>
                                                                    <Table.Cell>{record.message}</Table.Cell>
                                                                    <Table.Cell>{record.status}</Table.Cell>
                                                                    <Table.Cell>{record.created_at}</Table.Cell>
                                                                    <Table.Cell>
                                                                        <Link to={`/sys_msg/${record.id}`}>
                                                                            {viewButton}
                                                                        </Link>
                                                                    </Table.Cell>
                                                                </Table.Row>
                                                            ))}
                                                        </Table.Body>
                                                    </Table>

                                                    <br />

                                                    <div className="container">
                                                        {/* <CommentList data={this.state.data} /> */}
                                                        <nav aria-label="Page navigation example">
                                                            <ReactPaginate
                                                                previousLabel={'Previous'}
                                                                nextLabel={'Next'}
                                                                breakLabel={'...'}
                                                                breakClassName={'break-me'}
                                                                pageCount={numberOfPages}
                                                                marginPagesDisplayed={2}
                                                                pageRangeDisplayed={10}
                                                                onPageChange={this.handlePageClick}
                                                                containerClassName={'pagination'}
                                                                subContainerClassName={'pages pagination page-item'}
                                                                activeClassName={'active'}
                                                                pageClassName="page-item"
                                                                pageLinkClassName="page-link"
                                                                disabledClassName="disabled"
                                                                nextClassName="page-link"
                                                                previousClassName="page-link"
                                                            />
                                                        </nav>
                                                    </div>

                                                </CardBody>
                                            </Card>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            <FixedPlugin
                                bgColor={this.state.backgroundColor}
                                activeColor={this.state.activeColor}
                                handleActiveClick={this.handleActiveClick}
                                handleBgClick={this.handleBgClick}
                            />

                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

export default SysMsgs;
