import React from "react";
import 'react-widgets/dist/css/react-widgets.css';
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

// import {
//   Card,
//   CardHeader,
//   CardBody,
//   CardTitle,
//   Row,
//   Col
// } from "reactstrap";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import { getUserDetails, isLoggedIn, getAccessToken } from "../../../util/AuthService";
// import { AreaChart } from 'react-chartkick';
import { Link } from "react-router-dom";


class Reports extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Reports";
    this.state = {
      activePage: "dashboard",
      backgroundColor: "black",
      activeColor: "warning",
      invoicesChartData: []
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };


  componentDidMount() {

    this.checkAuth();
    var loggedIn = getUserDetails();
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = getAccessToken();
    }

    axios.defaults.headers.common["x-mb-token"] = securityToken;

    axios({
      url: API_BASE_URL + "/analytics/dashboard",
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));

        if (!response.data.error) {
          this.setState({ records: json_data });
          this.setState({ graphArray: json_data });

          this.setState({
            // members_count: json_data.members_count.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'),
            members_count: json_data.members_count,
            transactions_count: json_data.transactions_count,
            loan_types_count: json_data.loan_types_count,
            ussd_logs_count: json_data.ussd_logs_count
          });
        }
      })
      .catch(err => {
        console.log(err);
      });


    // // Retrieve Invoices Chart Data 
    // axios({
    //   url: API_BASE_URL + "/dashboard/invoices/chart",
    //   method: "GET",
    //   withCredentials: false
    // })
    //   .then(response => {
    //     var json_data = JSON.parse(JSON.stringify(response.data));

    //     if (!response.data.error) {
    //       this.setState({ invoicesChartData: json_data });
    //     }
    //   })
    //   .catch(err => {
    //     console.log(err);
    //   });

  }

  componentWillMount() {
    // this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");
    } else {
      var userDetails = getUserDetails();
      this.setState({
        name: userDetails.name,
        phone: userDetails.msisdn,
        email: userDetails.email
      });
    }
  }

  render() {

    const { members_count, transactions_count, loan_types_count,
      ussd_logs_count, invoicesChartData } = this.state;

    var arr = {};

    invoicesChartData.forEach(processData);
    function processData(record, index) {
      var babyArr = {};
      babyArr[0] = record.date;
      babyArr[1] = record.records_count;
      arr[record.date] = record.records_count;
    }

    return (
      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

        {loadHeader()}

        <div className="app-body">

          {loadSidebarMenu("/dashboard")}

          <main className="main">
            {/* <!-- Breadcrumb--> */}
            <ol className="breadcrumb">
              <li className="breadcrumb-item">Home</li>
              <li className="breadcrumb-item">
                <Link to="dashboard">Admin</Link>
              </li>
              <li className="breadcrumb-item active">Reports</li>
              {/* <!-- Breadcrumb Menu--> */}
              <li className="breadcrumb-menu d-md-down-none">
                <div className="btn-group" role="group" aria-label="Button group">
                  <Link className="btn" to="dashboard">
                    <i className="icon-speech"></i>
                  </Link>
                  <Link className="btn" to="dashboard">
                    <i className="icon-graph"></i>  Reports</Link>
                  <Link className="btn" to="profile">
                    <i className="icon-settings"></i> Profile</Link>
                </div>
              </li>
            </ol>
            <div className="container-fluid">
              <div className="animated fadeIn">

                <div className="row">
                  <div className="col-sm-6 col-lg-3">
                    <div className="card">
                      <div className="card-body">
                        <div className="h4 m-0">{members_count}</div>
                        <div>No. Of Members</div>
                        <div className="progress progress-xs my-1">
                          <div className="progress-bar bg-success" role="progressbar" style={{ width: "25%", ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                        </div>
                        <small className="text-muted">Count of all members.</small>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-lg-3">
                    <div className="card">
                      <div className="card-body">
                        <div className="h4 m-0"> {transactions_count} </div>
                        <div>Transactions Count</div>
                        <div className="progress progress-xs my-1">
                          <div className="progress-bar bg-info" role="progressbar" style={{ width: "25%", ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                        </div>
                        <small className="text-muted">Count of all USSD transactions.</small>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-lg-3">
                    <div className="card">
                      <div className="card-body">
                        <div className="h4 m-0"> {loan_types_count} </div>
                        <div>Loan Types Count</div>
                        <div className="progress progress-xs my-1">
                          <div className="progress-bar bg-warning" role="progressbar" style={{ width: "25%", ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                        </div>
                        <small className="text-muted">Number of available Loan Types.</small>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 col-lg-3">
                    <div className="card">
                      <div className="card-body">
                        <div className="h4 m-0"> {ussd_logs_count} </div>
                        <div>USSD Logs Count</div>
                        <div className="progress progress-xs my-1">
                          <div className="progress-bar bg-danger" role="progressbar" style={{ width: "25%", ariaValuenow: "25", ariaValuemin: "0", ariaValueMax: "100" }} ></div>
                        </div>
                        <small className="text-muted">Count of USSD Logs</small>
                      </div>
                    </div>
                  </div>
                </div>

                <br />

                {/* <Row>
                  <Col xs={12} sm={12} md={12}>
                    <Card>
                      <CardHeader>
                        <CardTitle>Transactions Processing</CardTitle>
                        <p className="card-category">Latest Transactions Processing</p>
                      </CardHeader>
                      <CardBody>
                        <AreaChart data={arr} />
                      </CardBody>
                    </Card>
                  </Col>
                </Row> */}

                <br />

              </div>
            </div>
          </main>
        </div>
        <footer className="app-footer">
          <div>
            <a href="http://strutstechnology.co.ke">Maxider Banking tool. </a>
            <span>&copy; Maxider Technology.</span>
          </div>
          <div className="ml-auto">
            <span>Powered by</span>
            <a href="http://strutstechnology.co.ke"> Maxider Technology.</a>
          </div>
        </footer>
      </div>
    );
  }
}

export default Reports;
