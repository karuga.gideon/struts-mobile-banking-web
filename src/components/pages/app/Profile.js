import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import {
  isLoggedIn,
  getUserDetails,
  getAccessToken
} from "../../../util/AuthService";

import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import { Form, Label } from "semantic-ui-react";
import axios from "axios";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Profile";

    this.state = {
      user_details: [],
      isLoading: false,
      pageOfItems: [],
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {

    this.checkAuth();
    var loggedIn = getUserDetails();
    console.log("loggedIn >>>> " + loggedIn.id);
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = getAccessToken();
    }


    axios.defaults.headers.common["x-mb-token"] = securityToken;

    var userID = loggedIn.id;
    console.log("Getting User Details >>> " + userID);

    axios({
      url: `${API_BASE_URL}/users/` + userID,
      method: "GET",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));


        if (!response.data.error) {
          this.setState({
            user_details: json_data,
            userUpdateName: json_data.first_name + " " + json_data.last_name,
            userUpdateFirstName: json_data.first_name,
            userUpdateLastName: json_data.last_name,
            userUpdatePhone: json_data.msisdn,
            userUpdateEmail: json_data.email,
            userUpdateUsername: json_data.username,
            userID: userID
          });

        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });

  }

  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {

    var loggedIn = isLoggedIn();
    console.log("loggedIn >>> " + loggedIn);

    if (!loggedIn) {
      this.props.history.push("/");
    } else {
      var userDetails = getUserDetails();
      this.setState({
        name: userDetails.first_name + " " + userDetails.last_name,
        firstname: this.state.first_name,
        lastname: this.state.last_name,
        phone: this.state.msisdn,
        msisdn: this.state.msisdn,
        email: this.state.email,
        created_at: this.state.created_at,
        user_id: userDetails.id
      });
    }
  }


  onSubmit = e => {

    this.setState({ isLoading: true });
    const { user_details, userID } = this.state;

    var userPayLoad = {
      name: this.state.userUpdateName,
      firstname: this.state.userUpdateFirstName,
      lastname: this.state.userUpdateLastName,
      msisdn: this.state.userUpdatePhone,
      email: this.state.userUpdateEmail,
      username: this.state.userUpdateUsername
    };

    var loggedIn = getUserDetails();


    var securityToken = "";

    if (loggedIn != null) {
      securityToken = getAccessToken();
    }


    axios.defaults.headers.common["x-mb-token"] = securityToken;

    console.log("Updating User ID Details >>> " + userID);
    console.log(user_details);

    axios({
      url: `${API_BASE_URL}/users/` + userID,
      method: "PUT",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: userPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });

        if (!response.data.error) {
          alert("User Updated Successfully.");
          this.props.history.push("/profile");
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };


  render() {

    const { user_id, user_details, userUpdateFirstName, userUpdateLastName, userUpdatePhone, userUpdateEmail } = this.state;

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
        <Form.Field
          primary
          className="field btn btn-primary btn-xl text-uppercase"
          type="submit"
          color="green"
          style={{ marginLeft: "18px" }}
          onClick={this.onSubmit}
        >
          Update Profile
        </Form.Field>
      );
    return (

      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

        { loadHeader()}

        <div className="app-body">

          {loadSidebarMenu()}

          <main className="main">

            <div
              className="main-panel"
              ref="mainPanel"
              style={{ overflowX: "hidden", msOverflowY: "hidden" }}
            >


              {/* <!-- Breadcrumb--> */}
              <ol className="breadcrumb">
                <li className="breadcrumb-item">Home</li>
                <li className="breadcrumb-item">
                  <Link to="dashboard">Admin</Link>
                </li>
                <li className="breadcrumb-item active">Profile</li>
                {/* <!-- Breadcrumb Menu--> */}
                <li className="breadcrumb-menu d-md-down-none">
                  <div className="btn-group" role="group" aria-label="Button group">
                    <Link className="btn" to="dashboard">
                      <i className="icon-speech"></i>
                    </Link>
                    <Link className="btn" to="dashboard">
                      <i className="icon-graph"></i>  Dashboard</Link>
                    <Link className="btn" to="users">
                      <i className="icon-settings"></i>  Settings</Link>
                  </div>
                </li>
              </ol>
              <div className="container-fluid">

                <Row>
                  <Col md={8} xs={8}>
                    <Card className="card-user">
                      <CardHeader>
                        <CardTitle>Update Profile</CardTitle>
                      </CardHeader>
                      <CardBody>

                        <Form>
                          <Label
                            style={{
                              fontSize: "0.8571em",
                              marginBottom: "5px",
                              color: "#9A9A9A"
                            }}
                          >
                            First Name
                      </Label>
                          <Form.Field className="form-control">
                            <input
                              type="text"
                              placeholder="First Name*"
                              value={userUpdateFirstName}
                              style={{
                                textDecoration: "none",
                                padding: "0px",
                                border: "0px"
                              }}
                              onChange={e =>
                                this.setState({ userUpdateFirstName: e.target.value })
                              }
                            />
                          </Form.Field>
                          <br />

                          <Label
                            style={{
                              fontSize: "0.8571em",
                              marginBottom: "5px",
                              color: "#9A9A9A"
                            }}
                          >
                            Last Name
                      </Label>
                          <Form.Field className="form-control">
                            <input
                              type="text"
                              placeholder="Last Name*"
                              value={userUpdateLastName}
                              style={{
                                textDecoration: "none",
                                padding: "0px",
                                border: "0px"
                              }}
                              onChange={e =>
                                this.setState({ userUpdateLastName: e.target.value })
                              }
                            />
                          </Form.Field>
                          <br />

                          <Label
                            style={{
                              fontSize: "0.8571em",
                              marginBottom: "5px",
                              color: "#9A9A9A"
                            }}
                          >
                            Phone Number
                      </Label>
                          <Form.Field className="form-control">
                            <input
                              type="text"
                              placeholder="Phone Number*"
                              value={userUpdatePhone}
                              style={{
                                textDecoration: "none",
                                padding: "0px",
                                border: "0px"
                              }}
                              onChange={e =>
                                this.setState({ userUpdatePhone: e.target.value })
                              }
                            />
                          </Form.Field>
                          <br />

                          <Label
                            style={{
                              fontSize: "0.8571em",
                              marginBottom: "5px",
                              color: "#9A9A9A"
                            }}
                          >
                            Email
                      </Label>
                          <Form.Field className="form-control">
                            <input
                              type="text"
                              placeholder="Email*"
                              value={userUpdateEmail}
                              style={{
                                textDecoration: "none",
                                padding: "0px",
                                border: "0px"
                              }}
                              onChange={e =>
                                this.setState({ userUpdateEmail: e.target.value })
                              }
                            />
                          </Form.Field>
                          <br />

                          <Row>
                            <div className="">
                              {button} &nbsp;
                          <br />
                            </div>
                          </Row>
                        </Form>

                      </CardBody>
                    </Card>
                  </Col>

                  <Col md={4} xs={12}>
                    <Card className="card-user">
                      <CardHeader>
                        <CardTitle>User Profile</CardTitle>
                      </CardHeader>
                      <CardBody>
                        <div className="description" style={{}}>

                          Name : {user_details.first_name} {user_details.last_name} <br />
                      Phone Number : {user_details.msisdn} <br />
                      Email Address: {user_details.email} <br />
                      Date Created : {user_details.created_at}  <br />
                      User ID : {user_id}

                          <br />
                          <hr />

                          <div style={{ textAlign: "center" }}>
                            <Link
                              to="/change-password"
                              className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                              style={{ width: "70%" }}
                            >
                              <Menu.Item name="Change Password" />
                            </Link>
                          </div>
                          <hr />

                        </div>
                      </CardBody>
                      <CardFooter />
                    </Card>

                  </Col>

                </Row>
              </div>

              <FixedPlugin
                bgColor={this.state.backgroundColor}
                activeColor={this.state.activeColor}
                handleActiveClick={this.handleActiveClick}
                handleBgClick={this.handleBgClick}
              />

            </div>
          </main>
        </div>
      </div>
    );
  }
}

export default Profile;
