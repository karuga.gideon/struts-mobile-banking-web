import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";
import { Link } from "react-router-dom";

class About extends React.Component {
  constructor(props) {
    super(props);
    document.title = "About";
    this.state = {
      records: [],
      isLoading: false,
      pageOfItems: [],
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };
  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };
  render() {

    return (

      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

        {loadHeader()}

        <div className="app-body">

          {loadSidebarMenu()}

          <main className="main">

            <div className="main-panel" ref="mainPanel">

              {/* <!-- Breadcrumb--> */}
              <ol className="breadcrumb">
                <li className="breadcrumb-item">Home</li>
                <li className="breadcrumb-item">
                  <Link to="dashboard">Admin</Link>
                </li>
                <li className="breadcrumb-item active">About</li>
                {/* <!-- Breadcrumb Menu--> */}
                <li className="breadcrumb-menu d-md-down-none">
                  <div className="btn-group" role="group" aria-label="Button group">
                    <Link className="btn" to="dashboard">
                      <i className="icon-speech"></i>
                    </Link>
                    <Link className="btn" to="dashboard">
                      <i className="icon-graph"></i>  Dashboard</Link>
                    <Link className="btn" to="users">
                      <i className="icon-settings"></i>  Settings</Link>
                  </div>
                </li>
              </ol>
              <div className="container-fluid">

                <Row>
                  <Col md={8} xs={12}>
                    <Card className="card-user">
                      <CardHeader>
                        <CardTitle>About the Maxider Banking tool</CardTitle>
                      </CardHeader>
                      <CardBody>
                        <div>
                          The Maxider Banking portal is a tool that gives information on inovices processed via the
                          repsective custom ESD Software solution provided to the client. It shows summaries as well
                          as detailed information regarding the invoices processed with their respective signatures.
                      <br />
                          <hr />
                        </div>
                      </CardBody>
                    </Card>
                  </Col>
                  <Col md={4} xs={12}>
                    <Card className="card-user">
                      <CardHeader>
                        <CardTitle>About the Maxider Banking tool</CardTitle>
                      </CardHeader>
                      <CardBody>
                        <div className="list-group">
                          <Link to="/about" className="list-group-item list-group-item-action active">
                            About
                      </Link>
                          <Link to="/members" className="list-group-item list-group-item-action">Members </Link>
                          <Link to="/transactions" className="list-group-item list-group-item-action">Transactions</Link>
                          <Link to="/ussd_logs" className="list-group-item list-group-item-action">USSD Logs</Link>
                          <Link to="/users" className="list-group-item list-group-item-action">Users</Link>
                          <Link to="/profile" className="list-group-item list-group-item-action">Profile</Link>
                          <Link to="/dashboard" className="list-group-item list-group-item-action">Dashboard</Link>
                        </div>
                      </CardBody>
                      <CardFooter />
                    </Card>
                  </Col>
                </Row>
              </div>

              <FixedPlugin
                bgColor={this.state.backgroundColor}
                activeColor={this.state.activeColor}
                handleActiveClick={this.handleActiveClick}
                handleBgClick={this.handleBgClick}
              />
            </div>
          </main>
        </div>
      </div>
    );
  }
}

export default About;
