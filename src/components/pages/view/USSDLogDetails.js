import React from "react";
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    CardFooter,
    Row,
    Col
} from "reactstrap";

import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Form, Label } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import { isLoggedIn, getUserDetails, getAccessToken } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants"; 
import axios from "axios";

class USSDLogDetails extends React.Component {
    constructor(props) {
        super(props);
        document.title = "USSDLog Details";

        var ussdLogID = this.props.match.params.ussdLogID;
        console.log("ussdLogID >>> " + ussdLogID);

        this.state = {
            records: [],
            user_details: {},
            isLoading: false,
            pageOfItems: [],
            ussdLogID: ussdLogID,
            backgroundColor: "black",
            activeColor: "warning"
        };
    }

    handleActiveClick = color => {
        this.setState({ activeColor: color });
    };

    handleBgClick = color => {
        this.setState({ backgroundColor: color });
    };

    componentDidMount() {

        this.checkAuth();
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        const { ussdLogID } = this.state;
        console.log("Getting USSDLog Details >>> " + ussdLogID);

        axios({
            url: `${API_BASE_URL}/ussd_logs/` + ussdLogID,
            method: "GET",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                var json_data = JSON.parse(JSON.stringify(response.data));


                if (!response.data.error) {
                    this.setState({
                        user_details: json_data,
                        action: json_data.action,
                        userUpdateNationalID: json_data.national_id,
                        description: json_data.description,
                        msisdn: json_data.msisdn,
                        member_id: json_data.member_id,
                        userUpdateUsername: json_data.username
                    });

                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    }


    componentWillMount() {
        this.checkAuth();
    }

    checkAuth() {
        var loggedIn = isLoggedIn();
        if (!loggedIn) {
            this.props.history.push("/");
        } else {
            var userDetails = getUserDetails();
            this.setState({
                name: userDetails.name,
                phone: userDetails.msisdn,
                email: userDetails.email
            });
        }
    }


    onSubmit = e => {

        this.setState({ isLoading: true });
        const { user_details, ussdLogID } = this.state;

        var userPayLoad = {
            action: this.state.action,
            description: this.state.msisdn,
            msisdn: this.state.member_id,
            username: this.state.userUpdateUsername
        };

        var loggedIn = getUserDetails();


        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        console.log("Updating USSDLog ID Details >>> " + ussdLogID);
        console.log(user_details);

        axios({
            url: `${API_BASE_URL}/ussd_logs/` + ussdLogID + `/reset_pin`,
            method: "PUT",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            },
            data: userPayLoad
        })
            .then(response => {
                this.setState({ isLoading: false });

                if (!response.data.error) {
                    alert("SMS Sent Successfully.");
                    this.props.history.push("/ussd_logs");
                } else {
                    alert(response.data.details);
                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    };

    render() {

        const { ussdLogID, user_details, action, description, msisdn, member_id } = this.state;

        return (

            <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

                { loadHeader()}

                <div className="app-body">

                    {loadSidebarMenu()}

                    <main className="main">

                        <div
                            className="main-panel"
                            ref="mainPanel"
                            style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                        >


                            {/* <!-- Breadcrumb--> */}
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">Home</li>
                                <li className="breadcrumb-item">
                                    <a href="dashboard">Admin</a>
                                </li>
                                <li className="breadcrumb-item active">USSDLog Details</li>
                                {/* <!-- Breadcrumb Menu--> */}
                                <li className="breadcrumb-menu d-md-down-none">
                                    <div className="btn-group" role="group" aria-label="Button group">
                                        <a className="btn" href="dashboard">
                                            <i className="icon-speech"></i>
                                        </a>
                                        <a className="btn" href="dashboard">
                                            <i className="icon-graph"></i>  Dashboard</a>
                                        <a className="btn" href="users">
                                            <i className="icon-settings"></i>  Settings</a>
                                    </div>
                                </li>
                            </ol>
                            <div className="container-fluid">

                                <Row>
                                    <Col md={8} xs={8}>
                                        <Card className="card-user">
                                            <CardHeader>
                                                <CardTitle>USSDLog Details</CardTitle>
                                            </CardHeader>
                                            <CardBody>
                                                <Form>
                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Action
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Action*"
                                                            className=""
                                                            value={action}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ action: e.target.value })
                                                            }
                                                            readOnly 
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Description
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Description*"
                                                            className=""
                                                            value={description}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                           
                                                            readOnly
                                                        />
                                                    </Form.Field>
                                                    <br />


                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        MSISDN
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="MSISDN*"
                                                            className=""
                                                            value={msisdn}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ msisdn: e.target.value })
                                                            }
                                                            readOnly
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Member ID
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Member ID*"
                                                            className=""
                                                            value={member_id}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}                                                            
                                                            readOnly
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                </Form>
                                            </CardBody>
                                        </Card>
                                    </Col>

                                    <Col md={4} xs={12}>
                                        <Card className="card-user">

                                            <CardHeader>
                                                <CardTitle>USSDLog Details</CardTitle>
                                            </CardHeader>

                                            <CardBody>
                                                <p className="description" style={{ marginTop: "20px" }}>

                                                Action : {action} <br />
                      Description : {description} <br /> 
                      MSISDN   : {msisdn} <br /> 
                      Date Created : {user_details.created_at} <br />
                      USSDLog ID : {ussdLogID} <br />

                                                    <hr />

                                                    <div style={{ textAlign: "center" }}>
                                                        <Link
                                                            to="/ussd_logs"
                                                            className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                                                            style={{ width: "70%" }}
                                                        >
                                                            <Menu.Item name="Back" />
                                                        </Link>
                                                    </div>
                                                </p>

                                            </CardBody>
                                            <CardFooter />
                                        </Card>
                                    </Col>
                                </Row>
                            </div>

                            <FixedPlugin
                                bgColor={this.state.backgroundColor}
                                activeColor={this.state.activeColor}
                                handleActiveClick={this.handleActiveClick}
                                handleBgClick={this.handleBgClick}
                            />
                        </div>
                    </main>
                </div>
            </div>
        );
    }
}

export default USSDLogDetails;
