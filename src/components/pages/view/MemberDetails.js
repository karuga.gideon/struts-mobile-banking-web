import React from "react";
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    CardFooter,
    Row,
    Col
} from "reactstrap";

import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Form, Label } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import { isLoggedIn, getUserDetails, getAccessToken } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import axios from "axios";

class MemberDetails extends React.Component {
    constructor(props) {
        super(props);
        document.title = "Member Details";

        var memberID = this.props.match.params.memberID;
        console.log("memberID >>> " + memberID);

        this.state = {
            records: [],
            user_details: {},
            isLoading: false,
            pageOfItems: [],
            memberID: memberID,
            backgroundColor: "black",
            activeColor: "warning"
        };
    }

    handleActiveClick = color => {
        this.setState({ activeColor: color });
    };

    handleBgClick = color => {
        this.setState({ backgroundColor: color });
    };

    componentDidMount() {

        this.checkAuth();
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        const { memberID } = this.state;
        console.log("Getting Member Details >>> " + memberID);

        axios({
            url: `${API_BASE_URL}/members/` + memberID,
            method: "GET",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                var json_data = JSON.parse(JSON.stringify(response.data));


                if (!response.data.error) {
                    this.setState({
                        user_details: json_data,
                        userUpdateName: json_data.name,
                        userUpdateNationalID: json_data.national_id,
                        userUpdatePhone: json_data.msisdn,
                        userUpdateEmail: json_data.email,
                        userUpdateUsername: json_data.username
                    });

                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    }


    componentWillMount() {
        this.checkAuth();
    }

    checkAuth() {
        var loggedIn = isLoggedIn();
        if (!loggedIn) {
            this.props.history.push("/");
        } else {
            var userDetails = getUserDetails();
            this.setState({
                name: userDetails.name,
                phone: userDetails.msisdn,
                email: userDetails.email
            });
        }
    }


    onSubmit = e => {

        this.setState({ isLoading: true });
        const { user_details, memberID } = this.state;

        var userPayLoad = {
            name: this.state.userUpdateName,
            msisdn: this.state.userUpdatePhone,
            email: this.state.userUpdateEmail,
            username: this.state.userUpdateUsername
        };

        var loggedIn = getUserDetails();


        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        console.log("Updating Member ID Details >>> " + memberID);
        console.log(user_details);

        axios({
            url: `${API_BASE_URL}/members/` + memberID + `/reset_pin`,
            method: "PUT",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            },
            data: userPayLoad
        })
            .then(response => {
                this.setState({ isLoading: false });

                if (!response.data.error) {
                    alert("SMS Sent Successfully.");
                    this.props.history.push("/sys_msgs");
                } else {
                    alert(response.data.details);
                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    };

    render() {

        const { memberID, user_details, userUpdateName, userUpdatePhone, userUpdateEmail } = this.state;

        let resetPinButton = this.state.isLoading ? (
            <Loader />
        ) : (
                <Form.Field
                    primary
                    className="field btn btn-primary btn-xl text-uppercase"
                    type="submit"
                    color="green"
                    style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
                    onClick={this.onSubmit}
                >
                    Reset PIN
                </Form.Field>
            );

        return (

            <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

                { loadHeader()}

                <div className="app-body">

                    {loadSidebarMenu()}

                    <main className="main">

                        <div
                            className="main-panel"
                            ref="mainPanel"
                            style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                        >


                            {/* <!-- Breadcrumb--> */}
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">Home</li>
                                <li className="breadcrumb-item">
                                    <a href="dashboard">Admin</a>
                                </li>
                                <li className="breadcrumb-item active">Member Details</li>
                                {/* <!-- Breadcrumb Menu--> */}
                                <li className="breadcrumb-menu d-md-down-none">
                                    <div className="btn-group" role="group" aria-label="Button group">
                                        <a className="btn" href="dashboard">
                                            <i className="icon-speech"></i>
                                        </a>
                                        <a className="btn" href="dashboard">
                                            <i className="icon-graph"></i>  Dashboard</a>
                                        <a className="btn" href="users">
                                            <i className="icon-settings"></i>  Settings</a>
                                    </div>
                                </li>
                            </ol>
                            <div className="container-fluid">

                                <Row>
                                    <Col md={8} xs={8}>
                                        <Card className="card-user">
                                            <CardHeader>
                                                <CardTitle>Member Details</CardTitle>
                                            </CardHeader>
                                            <CardBody>
                                                <Form>
                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Name
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Name*"
                                                            className=""
                                                            value={userUpdateName}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ userUpdateName: e.target.value })
                                                            }
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Phone Number
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Phone Number*"
                                                            className=""
                                                            value={userUpdatePhone}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ userUpdatePhone: e.target.value })
                                                            }
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Email
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Email*"
                                                            className=""
                                                            value={userUpdateEmail}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ userUpdateEmail: e.target.value })
                                                            }
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Row>
                                                        <div className="">
                                                            {resetPinButton} &nbsp;
                          <br />
                                                        </div>
                                                    </Row>
                                                </Form>
                                            </CardBody>
                                        </Card>
                                    </Col>

                                    <Col md={4} xs={12}>
                                        <Card className="card-user">

                                            <CardHeader>
                                                <CardTitle>Member Details</CardTitle>
                                            </CardHeader>

                                            <CardBody>
                                                <p className="description" style={{ marginTop: "20px" }}>

                                                    Name : {userUpdateName} <br />
                      Phone Number : {userUpdatePhone} <br />
                      Email Address: {userUpdateEmail} <br />
                      Date Created : {user_details.created_at} <br />
                      Member ID : {memberID} <br />

                                                    <hr />

                                                    <div style={{ textAlign: "center" }}>
                                                        <Link
                                                            to="/members"
                                                            className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                                                            style={{ width: "70%" }}
                                                        >
                                                            <Menu.Item name="Back" />
                                                        </Link>
                                                    </div>
                                                </p>

                                            </CardBody>
                                            <CardFooter />
                                        </Card>
                                    </Col>
                                </Row>
                            </div>

                            <FixedPlugin
                                bgColor={this.state.backgroundColor}
                                activeColor={this.state.activeColor}
                                handleActiveClick={this.handleActiveClick}
                                handleBgClick={this.handleBgClick}
                            />
                        </div>
                    </main>
                </div>
            </div>
        );
    }
}

export default MemberDetails;
