import React from "react";
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    CardFooter,
    Row,
    Col
} from "reactstrap";

import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Form, Label } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import { isLoggedIn, getUserDetails, getAccessToken } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants"; 
import axios from "axios";

class SysMsgDetails extends React.Component {
    constructor(props) {
        super(props);
        document.title = "SysMsg Details";

        var sysMsgID = this.props.match.params.sysMsgID;
        console.log("sysMsgID >>> " + sysMsgID);

        this.state = {
            records: [],
            user_details: {},
            isLoading: false,
            pageOfItems: [],
            sysMsgID: sysMsgID,
            backgroundColor: "black",
            activeColor: "warning"
        };
    }

    handleActiveClick = color => {
        this.setState({ activeColor: color });
    };

    handleBgClick = color => {
        this.setState({ backgroundColor: color });
    };

    componentDidMount() {

        this.checkAuth();
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        const { sysMsgID } = this.state;
        console.log("Getting SysMsg Details >>> " + sysMsgID);

        axios({
            url: `${API_BASE_URL}/sys_msgs/` + sysMsgID,
            method: "GET",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                var json_data = JSON.parse(JSON.stringify(response.data));


                if (!response.data.error) {
                    this.setState({
                        user_details: json_data,
                        message_type: json_data.message_type,
                        recipient: json_data.recipient,
                        message: json_data.message,
                        status: json_data.status,
                        userUpdateUsername: json_data.username
                    });

                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    }


    componentWillMount() {
        this.checkAuth();
    }

    checkAuth() {
        var loggedIn = isLoggedIn();
        if (!loggedIn) {
            this.props.history.push("/");
        } else {
            var userDetails = getUserDetails();
            this.setState({
                name: userDetails.name,
                phone: userDetails.msisdn,
                email: userDetails.email
            });
        }
    }


    onSubmit = e => {

        this.setState({ isLoading: true });
        const { user_details, sysMsgID } = this.state;

        var userPayLoad = {
            message_type: this.state.message_type,
            recipient: this.state.message,
            message: this.state.status,
            username: this.state.userUpdateUsername
        };

        var loggedIn = getUserDetails();


        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        console.log("Updating SysMsg ID Details >>> " + sysMsgID);
        console.log(user_details);

        axios({
            url: `${API_BASE_URL}/sys_msgs/` + sysMsgID + `/reset_pin`,
            method: "PUT",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            },
            data: userPayLoad
        })
            .then(response => {
                this.setState({ isLoading: false });

                if (!response.data.error) {
                    alert("SMS Sent Successfully.");
                    this.props.history.push("/sys_msgs");
                } else {
                    alert(response.data.details);
                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    };

    render() {

        const { sysMsgID, user_details, message_type, recipient, message, status } = this.state;

        return (

            <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

                { loadHeader()}

                <div className="app-body">

                    {loadSidebarMenu()}

                    <main className="main">

                        <div
                            className="main-panel"
                            ref="mainPanel"
                            style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                        >


                            {/* <!-- Breadcrumb--> */}
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">Home</li>
                                <li className="breadcrumb-item">
                                    <a href="dashboard">Admin</a>
                                </li>
                                <li className="breadcrumb-item active">SysMsg Details</li>
                                {/* <!-- Breadcrumb Menu--> */}
                                <li className="breadcrumb-menu d-md-down-none">
                                    <div className="btn-group" role="group" aria-label="Button group">
                                        <a className="btn" href="dashboard">
                                            <i className="icon-speech"></i>
                                        </a>
                                        <a className="btn" href="dashboard">
                                            <i className="icon-graph"></i>  Dashboard</a>
                                        <a className="btn" href="users">
                                            <i className="icon-settings"></i>  Settings</a>
                                    </div>
                                </li>
                            </ol>
                            <div className="container-fluid">

                                <Row>
                                    <Col md={8} xs={8}>
                                        <Card className="card-user">
                                            <CardHeader>
                                                <CardTitle>SysMsg Details</CardTitle>
                                            </CardHeader>
                                            <CardBody>
                                                <Form>
                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Message Type
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Message Type*"
                                                            className=""
                                                            value={message_type}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px", 
                                                                width: "100%"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ message_type: e.target.value })
                                                            }
                                                            readOnly 
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Recipient
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Recipient*"
                                                            className=""
                                                            value={recipient}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px", 
                                                                width: "100%"
                                                            }}
                                                           
                                                            readOnly
                                                        />
                                                    </Form.Field>
                                                    <br />


                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Message
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Message*"
                                                            className=""
                                                            value={message}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px", 
                                                                width: "100%"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ message: e.target.value })
                                                            }
                                                            readOnly
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Status
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Status*"
                                                            className=""
                                                            value={status}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px", 
                                                                width: "100%"
                                                            }}                                                            
                                                            readOnly
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                </Form>
                                            </CardBody>
                                        </Card>
                                    </Col>

                                    <Col md={4} xs={12}>
                                        <Card className="card-user">

                                            <CardHeader>
                                                <CardTitle>SysMsg Details</CardTitle>
                                            </CardHeader>

                                            <CardBody>
                                                <p className="description" style={{ marginTop: "20px" }}>

                                                Message Type : {message_type} <br />
                      Recipient : {recipient} <br /> 
                      Message   : {message} <br /> 
                      Date Created : {user_details.created_at} <br />
                      SysMsg ID : {sysMsgID} <br />

                                                    <hr />

                                                    <div style={{ textAlign: "center" }}>
                                                        <Link
                                                            to="/sys_msgs"
                                                            className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                                                            style={{ width: "70%" }}
                                                        >
                                                            <Menu.Item name="Back" />
                                                        </Link>
                                                    </div>
                                                </p>

                                            </CardBody>
                                            <CardFooter />
                                        </Card>
                                    </Col>
                                </Row>
                            </div>

                            <FixedPlugin
                                bgColor={this.state.backgroundColor}
                                activeColor={this.state.activeColor}
                                handleActiveClick={this.handleActiveClick}
                                handleBgClick={this.handleBgClick}
                            />
                        </div>
                    </main>
                </div>
            </div>
        );
    }
}

export default SysMsgDetails;
