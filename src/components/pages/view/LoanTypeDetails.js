import React from "react";
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    CardFooter,
    Row,
    Col
} from "reactstrap";

import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Form, Label } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import { isLoggedIn, getUserDetails, getAccessToken } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants";
import axios from "axios";

class LoanTypeDetails extends React.Component {
    constructor(props) {
        super(props);
        document.title = "Loan Type Details";

        var loanTypeID = this.props.match.params.loanTypeID;
        console.log("loanTypeID >>> " + loanTypeID);

        this.state = {
            records: [],
            user_details: {},
            isLoading: false,
            pageOfItems: [],
            loanTypeID: loanTypeID, 
            loan_name: "",
            min_loan_amount: 0,
            max_loan_amount: 0,
            installments: 1,
            status: "",
            backgroundColor: "black",
            activeColor: "warning"
        };
    }

    handleActiveClick = color => {
        this.setState({ activeColor: color });
    };

    handleBgClick = color => {
        this.setState({ backgroundColor: color });
    };

    componentDidMount() {

        this.checkAuth();
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        const { loanTypeID } = this.state;
        console.log("Getting Loan Type Details >>> " + loanTypeID);

        axios({
            url: `${API_BASE_URL}/loan_types/` + loanTypeID,
            method: "GET",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                var json_data = JSON.parse(JSON.stringify(response.data));


                if (!response.data.error) {
                    this.setState({
                        user_details: json_data,
                        loan_code: json_data.loan_code,
                        loan_name: json_data.loan_name,
                        min_loan_amount: json_data.min_loan_amount,
                        max_loan_amount: json_data.max_loan_amount,
                        installments: json_data.installments,
                        status: json_data.status
                    });

                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    }


    componentWillMount() {
        this.checkAuth();
    }

    checkAuth() {
        var loggedIn = isLoggedIn();
        if (!loggedIn) {
            this.props.history.push("/");
        } else {
            var userDetails = getUserDetails();
            this.setState({
                name: userDetails.name,
                phone: userDetails.msisdn,
                email: userDetails.email
            });
        }
    }


    activateLoanType = e => {

        this.setState({ isLoading: true });
        const { loanTypeID } = this.state;

        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;
        console.log("Activating Loan Type ID >>> " + loanTypeID);

        axios({
            url: `${API_BASE_URL}/loan_types/` + loanTypeID + `/activate`,
            method: "PUT",
            withCredentials: false
        })
            .then(response => {
                this.setState({ isLoading: false });

                if (!response.data.error) {
                    this.props.history.push("/loan_types");
                } else {
                    alert(response.data.details);
                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    };

    deActivateLoanType = e => {

        this.setState({ isLoading: true });
        const { user_details, loanTypeID } = this.state;

        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        console.log("Deactivating Loan Type ID >>> " + loanTypeID);
        console.log(user_details);

        axios({
            url: `${API_BASE_URL}/loan_types/` + loanTypeID + `/deactivate`,
            method: "PUT",
            withCredentials: false
        })
            .then(response => {
                this.setState({ isLoading: false });

                if (!response.data.error) {
                    this.props.history.push("/loan_types");
                } else {
                    alert(response.data.details);
                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    };

    onSubmit = e => {

        this.setState({ isLoading: true });
        const { loanTypeID } = this.state;

        var loanTypePayLoad = {
            loan_code: this.state.loan_code,
            loan_name: this.state.loan_name,
            min_loan_amount: parseInt(this.state.min_loan_amount, 10),
            max_loan_amount: parseInt(this.state.max_loan_amount, 10),
            installments: parseInt(this.state.installments, 10) 
        };

        var loggedIn = getUserDetails();


        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        axios({
            url: `${API_BASE_URL}/loan_types/` + loanTypeID,
            method: "PUT",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            },
            data: loanTypePayLoad
        })
            .then(response => {
                this.setState({ isLoading: false });

                if (!response.data.error) {
                    alert("Loan type updated successfully.");
                    this.props.history.push("/loan_types");
                } else {
                    alert(response.data.details);
                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    };



    render() {

        const { loanTypeID, user_details, loan_code, loan_name, installments, min_loan_amount, max_loan_amount, status } = this.state;


        let updateLoanTypeButton = (
            <Form.Field
                primary
                className="field btn btn-primary btn-xl text-uppercase"
                type="submit"
                color="green"
                style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
                onClick={this.onSubmit}
            >
                Update
            </Form.Field>
        );

        let activateDeactivateButton = status === "active" ? (

            <Form.Field
                danger
                className="field btn btn-danger btn-xl text-uppercase"
                type="submit"
                color="red"
                style={{ marginLeft: "18px" }}
                onClick={this.deActivateLoanType}
            >
                Deactivate
            </Form.Field>

        ) : (
                <Form.Field
                    primary
                    className="field btn btn-primary btn-xl text-uppercase"
                    type="submit"
                    color="green"
                    style={{ backgroundColor: "#51cbce", marginLeft: "18px" }}
                    onClick={this.activateLoanType}
                >
                    Activate
                </Form.Field>
            );

        return (

            <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

                { loadHeader()}

                <div className="app-body">

                    {loadSidebarMenu()}

                    <main className="main">

                        <div
                            className="main-panel"
                            ref="mainPanel"
                            style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                        >


                            {/* <!-- Breadcrumb--> */}
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">Home</li>
                                <li className="breadcrumb-item">
                                    <a href="dashboard">Admin</a>
                                </li>
                                <li className="breadcrumb-item active">Loan Type Details</li>
                                {/* <!-- Breadcrumb Menu--> */}
                                <li className="breadcrumb-menu d-md-down-none">
                                    <div className="btn-group" role="group" aria-label="Button group">
                                        <a className="btn" href="dashboard">
                                            <i className="icon-speech"></i>
                                        </a>
                                        <a className="btn" href="dashboard">
                                            <i className="icon-graph"></i>  Dashboard</a>
                                        <a className="btn" href="users">
                                            <i className="icon-settings"></i>  Settings</a>
                                    </div>
                                </li>
                            </ol>
                            <div className="container-fluid">

                                <Row>
                                    <Col md={8} xs={8}>
                                        <Card className="card-user">
                                            <CardHeader>
                                                <CardTitle>Loan Type Details</CardTitle>
                                            </CardHeader>
                                            <CardBody>
                                                <Form>
                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Loan Code
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Loan Code*"
                                                            className=""
                                                            value={loan_code}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ loan_code: e.target.value })
                                                            }
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Loan Name
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Loan Name*"
                                                            className=""
                                                            value={loan_name}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ loan_name: e.target.value })
                                                            }
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Min Loan Amount
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Min Loan Amount*"
                                                            className=""
                                                            value={min_loan_amount}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ min_loan_amount: e.target.value })
                                                            }
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Max Loan Amount
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Max Loan Amount*"
                                                            className=""
                                                            value={max_loan_amount}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ max_loan_amount: e.target.value })
                                                            }
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Installments
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Installments*"
                                                            className=""
                                                            value={installments}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ installments: e.target.value })
                                                            }
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Row>
                                                        <div className="">
                                                            {updateLoanTypeButton}
                                                            {activateDeactivateButton}
                                                            <br />
                                                        </div>
                                                    </Row>
                                                </Form>
                                            </CardBody>
                                        </Card>
                                    </Col>

                                    <Col md={4} xs={12}>
                                        <Card className="card-user">

                                            <CardHeader>
                                                <CardTitle>Loan Type Details</CardTitle>
                                            </CardHeader>

                                            <CardBody>
                                                <p className="description" style={{ marginTop: "20px" }}>

                                                    Loan Code : {loan_code} <br />
                                                    Loan Name : {loan_name} <br />
                      Min Loan Amount : {min_loan_amount} <br />
                      Max Loan Amount : {max_loan_amount} <br />
                      Installments : {installments} <br />
                      Status : {status} <br />
                      Date Created : {user_details.created_at} <br />
                      Loan Type ID : {loanTypeID} <br />

                                                    <hr />

                                                    <div style={{ textAlign: "center" }}>
                                                        <Link
                                                            to="/loan_types"
                                                            className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                                                            style={{ width: "70%" }}
                                                        >
                                                            <Menu.Item name="Back" />
                                                        </Link>
                                                    </div>
                                                </p>

                                            </CardBody>
                                            <CardFooter />
                                        </Card>
                                    </Col>
                                </Row>
                            </div>

                            <FixedPlugin
                                bgColor={this.state.backgroundColor}
                                activeColor={this.state.activeColor}
                                handleActiveClick={this.handleActiveClick}
                                handleBgClick={this.handleBgClick}
                            />
                        </div>
                    </main>
                </div>
            </div>
        );
    }
}

export default LoanTypeDetails;
