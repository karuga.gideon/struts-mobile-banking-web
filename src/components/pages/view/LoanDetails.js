import React from "react";
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    CardFooter,
    Row,
    Col
} from "reactstrap";

import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Form, Label } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import { isLoggedIn, getUserDetails, getAccessToken } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants";
import axios from "axios";

class LoanDetails extends React.Component {
    constructor(props) {
        super(props);
        document.title = "Loan Details";

        var loanID = this.props.match.params.loanID;
        console.log("loanID >>> " + loanID);

        this.state = {
            records: [],
            user_details: {},
            isLoading: false,
            pageOfItems: [],
            loanID: loanID,
            backgroundColor: "black",
            activeColor: "warning", 
            loan: {}, 
            member: {} 
        };
    }

    handleActiveClick = color => {
        this.setState({ activeColor: color });
    };

    handleBgClick = color => {
        this.setState({ backgroundColor: color });
    };

    componentDidMount() {

        this.checkAuth();
        var loggedIn = getUserDetails();
        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        const { loanID } = this.state;
        console.log("Getting Loan Details >>> " + loanID);

        axios({
            url: `${API_BASE_URL}/loans/` + loanID,
            method: "GET",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                var json_data = JSON.parse(JSON.stringify(response.data));

                if (!response.data.error) {
                    this.setState({
                        user_details: json_data, 
                        userUpdateNationalID: json_data.national_id,
                        installments: json_data.installments,
                        amount: json_data.amount,
                        status_description: json_data.status_description,
                        userUpdateUsername: json_data.username, 
                        loan: json_data.loan, 
                        member: json_data.member 
                    });

                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    }


    componentWillMount() {
        this.checkAuth();
    }

    checkAuth() {
        var loggedIn = isLoggedIn();
        if (!loggedIn) {
            this.props.history.push("/");
        } else {
            var userDetails = getUserDetails();
            this.setState({
                name: userDetails.name,
                phone: userDetails.msisdn,
                email: userDetails.email
            });
        }
    }


    onSubmit = e => {

        this.setState({ isLoading: true });
        const { user_details, loanID } = this.state;

        var userPayLoad = {
            account_no: this.state.amount,
            installments: this.state.amount,
            amount: this.state.status_description,
            username: this.state.userUpdateUsername
        };

        var loggedIn = getUserDetails();


        var securityToken = "";

        if (loggedIn != null) {
            securityToken = getAccessToken();
        }


        axios.defaults.headers.common["x-mb-token"] = securityToken;

        console.log("Updating Loan ID Details >>> " + loanID);
        console.log(user_details);

        axios({
            url: `${API_BASE_URL}/loans/` + loanID + `/reset_pin`,
            method: "PUT",
            withCredentials: false,
            headers: {
                "Content-Type": "application/json"
            },
            data: userPayLoad
        })
            .then(response => {
                this.setState({ isLoading: false });

                if (!response.data.error) {
                    alert("SMS Sent Successfully.");
                    this.props.history.push("/loans");
                } else {
                    alert(response.data.details);
                }
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    };

    render() {

        const { loan, loanID, member, user_details, amount, installments, status_description } = this.state;

        return (

            <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

                { loadHeader()}

                <div className="app-body">

                    {loadSidebarMenu()}

                    <main className="main">

                        <div
                            className="main-panel"
                            ref="mainPanel"
                            style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                        >


                            {/* <!-- Breadcrumb--> */}
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">Home</li>
                                <li className="breadcrumb-item">
                                    <a href="dashboard">Admin</a>
                                </li>
                                <li className="breadcrumb-item active">Loan Details</li>
                                {/* <!-- Breadcrumb Menu--> */}
                                <li className="breadcrumb-menu d-md-down-none">
                                    <div className="btn-group" role="group" aria-label="Button group">
                                        <a className="btn" href="dashboard">
                                            <i className="icon-speech"></i>
                                        </a>
                                        <a className="btn" href="dashboard">
                                            <i className="icon-graph"></i>  Dashboard</a>
                                        <a className="btn" href="users">
                                            <i className="icon-settings"></i>  Settings</a>
                                    </div>
                                </li>
                            </ol>
                            <div className="container-fluid">

                                <Row>
                                    <Col md={8} xs={8}>
                                        <Card className="card-user">
                                            <CardHeader>
                                                <CardTitle>Loan Details</CardTitle>
                                            </CardHeader>
                                            <CardBody>
                                                <Form>

                                                <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Member Name
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Member Name*"
                                                            className=""
                                                            value={member.name}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px", 
                                                                width: "100%"
                                                            }}                                                            
                                                            readOnly
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Loan Name
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Loan Name*"
                                                            className=""
                                                            value={loan.loan_name}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px", 
                                                                width: "100%"
                                                            }}                                                            
                                                            readOnly
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Amount
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Amount*"
                                                            className=""
                                                            value={amount}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px", 
                                                                width: "100%"
                                                            }}
                                                            onChange={e =>
                                                                this.setState({ amount: e.target.value })
                                                            }
                                                            readOnly 
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Installments
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Installments*"
                                                            className=""
                                                            value={installments}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px", 
                                                                width: "100%"
                                                            }}
                                                           
                                                            readOnly
                                                        />
                                                    </Form.Field>
                                                    <br />


                                                  

                                                    <Label
                                                        style={{
                                                            fontSize: "0.8571em",
                                                            marginBottom: "5px",
                                                            color: "#9A9A9A"
                                                        }}
                                                    >
                                                        Status
                      </Label>
                                                    <Form.Field className="form-control">
                                                        <input
                                                            type="text"
                                                            placeholder="Status*"
                                                            className=""
                                                            value={status_description}
                                                            style={{
                                                                textDecoration: "none",
                                                                padding: "0px",
                                                                border: "0px", 
                                                                width: "100%"
                                                            }}                                                            
                                                            readOnly
                                                        />
                                                    </Form.Field>
                                                    <br />

                                                </Form>
                                            </CardBody>
                                        </Card>
                                    </Col>

                                    <Col md={4} xs={12}>
                                        <Card className="card-user">

                                            <CardHeader>
                                                <CardTitle>Loan Details</CardTitle>
                                            </CardHeader>

                                            <CardBody>
                                                <p className="description" style={{ marginTop: "20px" }}>

                                                Member Name : {member.name} <br />
                                                Member Phone : {member.msisdn} <br />
                                                Member Status : {member.status} <br /><br/>

                                                Loan Name : {loan.loan_name} <br />
                                                Loan Code : {loan.loan_code} <br />
                                                Amount : {amount} <br />
                      Installments : {installments} <br /> 
                      Date Created : {user_details.created_at} <br />
                      Loan ID : {loanID} <br />

                                                    <hr />

                                                    <div style={{ textAlign: "center" }}>
                                                        <Link
                                                            to="/loans"
                                                            className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                                                            style={{ width: "70%" }}
                                                        >
                                                            <Menu.Item name="Back" />
                                                        </Link>
                                                    </div>
                                                </p>

                                            </CardBody>
                                            <CardFooter />
                                        </Card>
                                    </Col>
                                </Row>
                            </div>

                            <FixedPlugin
                                bgColor={this.state.backgroundColor}
                                activeColor={this.state.activeColor}
                                handleActiveClick={this.handleActiveClick}
                                handleBgClick={this.handleBgClick}
                            />
                        </div>
                    </main>
                </div>
            </div>
        );
    }
}

export default LoanDetails;
