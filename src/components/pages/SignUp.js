import React from "react";
import Footer from "../shared/Footer";
import { Link } from "react-router-dom";
import { Grid, Form } from "semantic-ui-react";
import axios from "axios";
import { API_BASE_URL } from "../../constants";
import Loader from "../shared/Loader";

class App extends React.Component {
  constructor(props) {
    super(props);

    document.title = "Registration";

    this.state = {
      org_name: "",
      contact_person: "",
      phone_number: "",
      email: "",
      password: "",
      confirmPassword: "",
      isLoading: false
    };
  }

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  onSubmit = e => {
    if (this.state.password !== this.state.confirmPassword) {
      alert("Passwords must match!!!!!");
      return;
    }

    this.setState({ isLoading: true });

    var signupPayload = {
      org_name: this.state.org_name,
      contact_person: this.state.contact_person,
      phone_number: this.state.phone_number,
      email: this.state.email,
      password: this.state.password
    };
    console.log("Creating organization...");
    console.log(signupPayload);

    axios({
      url: `${API_BASE_URL}/organization/create`,
      method: "POST",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: signupPayload
    })
      .then(response => {
        this.setState({ isLoading: false });
        
        if (!response.data.error) {
          // alert("Your OTP Is : " + response.data.typeId);
          this.props.history.push("/signin");
          localStorage.setItem("OTP", response.data.typeId);
          // this.props.setUser({
          //   msisdn: response.data.msisdn,
          //   name: response.data.name
          // })
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {
    let button = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        // control={Button}
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#fed136" }}
        onClick={this.onSubmit}
      >
        Sign Up
      </Form.Field>
    );

    return (
      <div>
        {/* Navigation */}
        {/* <Navbar /> */}

        {/* Contact */}
        <section id="contact">
          <div className="container">
            <div className="row" style={{ marginTop: "-120px" }}>
              <div className="col-lg-12 text-center">
                <div id="mainNav">
                  <Link className="navbar-brand " to="/">
                    Smart Survey
                  </Link>
                </div>
                <h4 className="section-heading text-uppercase">Sign Up</h4>
                {/* <h3 className="section-subheading text-muted">
                      Kindly fill in all the details.
                    </h3> */}
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3" />
              <div className="col-lg-6">
                <Form>
                  <Form.Field className="form-control">
                    {/* <label>Oranization Name</label> */}
                    <input
                      type="text"
                      placeholder="Organization Name*"
                      className="form-control"
                      style={{
                        textDecoration: "none",
                        padding: "15px",
                        border: "0px"
                      }}
                      onChange={e =>
                        this.setState({ org_name: e.target.value })
                      }
                    />
                  </Form.Field>
                  <br />

                  <Form.Field className="form-control">
                    {/* <label>Contact Person</label> */}
                    <input
                      placeholder="Contact Person"
                      className="form-control"
                      style={{
                        textDecoration: "none",
                        padding: "15px",
                        border: "0px"
                      }}
                      onChange={e =>
                        this.setState({ contact_person: e.target.value })
                      }
                    />
                  </Form.Field>
                  <br />

                  <Form.Field className="form-control">
                    {/* <label>Phone Number</label> */}
                    <input
                      placeholder="Phone Number"
                      className="form-control"
                      style={{
                        textDecoration: "none",
                        padding: "15px",
                        border: "0px"
                      }}
                      onChange={e =>
                        this.setState({ phone_number: e.target.value })
                      }
                    />
                  </Form.Field>
                  <br />

                  <Form.Field className="form-control">
                    {/* <label>Email</label> */}
                    <input
                      type="email"
                      placeholder="email"
                      className="form-control"
                      style={{
                        textDecoration: "none",
                        padding: "15px",
                        border: "0px"
                      }}
                      onChange={e => this.setState({ email: e.target.value })}
                    />
                  </Form.Field>
                  <br />

                  <Form.Field className="form-control">
                    {/* <label>Password</label> */}
                    <input
                      type="password"
                      placeholder="password"
                      className="form-control"
                      style={{
                        textDecoration: "none",
                        padding: "15px",
                        border: "0px"
                      }}
                      onChange={e =>
                        this.setState({ password: e.target.value })
                      }
                    />
                  </Form.Field>
                  <br />

                  <Form.Field className="form-control">
                    {/* <label>Confirm Password</label> */}
                    <input
                      type="password"
                      placeholder="Confirm Password"
                      className="form-control"
                      style={{
                        textDecoration: "none",
                        padding: "15px",
                        border: "0px"
                      }}
                      onChange={e =>
                        this.setState({ confirmPassword: e.target.value })
                      }
                    />
                  </Form.Field>
                  <div className="col-lg-12 text-center">
                    <br />
                    {button}
                  </div>

                  <Grid>
                    <Grid.Column mobile={6} tablet={8} computer={16}>
                      <Grid.Column
                        mobile={6}
                        tablet={8}
                        computer={16}
                        floated="right"
                      />
                    </Grid.Column>
                  </Grid>
                </Form>

                <br />
                <h3 className="section-subheading text-muted text-center">
                  Already Registered? <Link to="/signin">Sign In</Link>
                </h3>
              </div>
            </div>
          </div>
        </section>
        {/* Footer */}
        <Footer />
        {/* Portfolio Modals */}

        {/* Bootstrap core JavaScript */}
        {/* Plugin JavaScript */}
        {/* Contact form JavaScript */}
        {/* Custom scripts for this template */}
      </div>
    );
  }
}

export default App;
