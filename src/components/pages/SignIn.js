import React from "react";
import { logout } from "../../util/AuthService";
import { Form } from "semantic-ui-react";
import axios from "axios";
import { API_BASE_URL, PROFILE_PAYLOAD } from "../../constants";
import Loader from "../shared/Loader";

import { FaUser, FaLock } from 'react-icons/fa';
import 'react-notifications/lib/notifications.css';


class App extends React.Component {

  constructor(props) {
    super(props);
    logout();

    document.title = "Maxider Banking : Sign In";

    this.state = {
      email: "",
      password: "",
      isLoading: false,
      displayErrorNotification: false
    };

  }

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  onSubmit = e => {

    this.setState({ isLoading: true });

    var loginPayLoad = {
      email: this.state.email,
      password: this.state.password,
    };

    axios({
      url: `${API_BASE_URL}/sessions`,
      method: "POST",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: loginPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });

        console.log(response);
        console.log(response.status);
        console.log(response.headers);

        if (response.status === 200) {
          console.log("Logging in user..");
          localStorage.setItem(PROFILE_PAYLOAD, JSON.stringify(response.data));
          localStorage.setItem("x-mb-token", response.headers["x-mb-token"]);
          this.props.history.push("/dashboard");
        } else {
          alert("Invalid Login!");
        }
      })
      .catch(error => {
        console.log("Encountered an error while logging in..");
        // alert("Invalid Login!");
        // NotificationManager.success('Success message', 'Title here');
        this.setState({ displayErrorNotification: true });
        this.setState({ email: "" });
        this.setState({ password: "" });
        // this.setState({ isLoading: false });
      });
  };


  render() {

    const { displayErrorNotification, email, password } = this.state;

    let button = this.state.isLoading ? (
      <Loader />
    ) : (
        <Form.Field
          // control={Button}
          primary
          className="field btn btn-primary btn-xl text-uppercase"
          type="submit"
          color="green"
          style={{ backgroundColor: "#fed136" }}
          onClick={this.onSubmit}
        >
          Sign In
        </Form.Field>
      );



    return (
      <div className="app flex-row align-items-center login_bg_image" style={{
        backgroundAttachment: "fixed",
        backgroundSize: "cover"
      }}>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-9">
              <div className="card-group">
                <div className="card p-4">
                  <Form>
                    <div className="card-body">

                      {
                        displayErrorNotification &&
                        <div className="alert alert-danger" role="alert"
                          onClick={() => this.setState({ displayErrorNotification: false })}>
                          Invalid Login!
                            </div>
                      }

                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-user"><FaUser /></i>
                          </span>
                        </div>
                        <input
                          type="text"
                          placeholder="email or email"
                          className="form-control"
                          value={email}
                          onChange={e => this.setState({ email: e.target.value })}
                        />
                      </div>
                      <div className="input-group mb-4">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="icon-lock"><FaLock /></i>
                          </span>
                        </div>
                        <input
                          type="password"
                          placeholder="password"
                          className="form-control"
                          value={password}
                          onChange={e =>
                            this.setState({ password: e.target.value })
                          }
                          onKeyPress={event => {
                            if (event.key === 'Enter') {
                              this.onSubmit()
                            }
                          }}
                        />
                      </div>
                      <div className="row">
                        <div className="col-6">
                          <Form.Field
                            primary
                            className="btn btn-primary px-4"
                            type="submit"
                            onClick={this.onSubmit}
                          >
                            Sign In
                            </Form.Field>
                        </div>
                        {/* <div className="col-6 text-right">
                            <button className="btn btn-link px-0" type="button">Forgot password?</button>
                          </div> */}
                      </div>
                    </div>
                  </Form>
                </div>
                <div className="card text-white bg-primary py-6 d-md-down-none" style={{ width: "44%" }}>
                  <div className="card-body text-center">
                    <div>
                      <h2>About</h2>
                      <p>The Maxider Mobile Banking portal is an administration tool to manage the mobile banking operations.
                      It allows for addition of new members as well as allowing for loan approvals and disbursements.
                      It also contains reporting and analytics.
                      </p>
                      <button className="btn btn-primary active mt-3" type="button">v2.1.4</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        {/* <!-- Icons--> */}
        <link href="node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet" />
        <link href="node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet" />
        <link href="node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
        <link href="node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" />

        {/* <!-- CoreUI and necessary plugins--> */}
        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="node_modules/pace-progress/pace.min.js"></script>
        <script src="node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
        <script src="node_modules/@coreui/coreui/dist/js/coreui.min.js"></script>
      </div>
    );
  }
}

export default App;
