import decode from "jwt-decode";
import history from "../util/history";
import {
  PROFILE_PAYLOAD,
  TOGGLE_LOADER,
  SURVEY_DETAILS
} from "../constants";

const ID_TOKEN_KEY = "id_token";
const ACCESS_TOKEN_KEY = "x-mb-token";

export function login() {
  clearIdToken();
  clearAccessToken();
  history.push("/signin");
}

export function logout() {
  clearIdToken();
  clearAccessToken();
  history.push("/");
}

export function requireAuth() {

  var auth_token = localStorage.getItem("x-mb-token");
  console.log("Auth token validation >>> " + auth_token);

  if (auth_token.length > 10) {
    console.log("User is logged in.");
  } else {
    console.log("User is not logged in, logging out...");
    logout();
  }
}

export function getIdToken() {
  return localStorage.getItem(ID_TOKEN_KEY);
}

export function getAccessToken() {
  return localStorage.getItem(ACCESS_TOKEN_KEY);
}

function clearIdToken() {
  localStorage.removeItem(PROFILE_PAYLOAD);
}

function clearAccessToken() {
  localStorage.removeItem(PROFILE_PAYLOAD);
  localStorage.removeItem("x-mb-token");
}

function getParameterByName(name) {
  var payload = JSON.parse(localStorage.getItem(PROFILE_PAYLOAD) || "{}");
  return payload.name || false;
}

export function getUserDetails() {
  var payload = JSON.parse(localStorage.getItem(PROFILE_PAYLOAD) || "{}");
  return payload;
}

export function getSurveyDetails() {
  var payload = JSON.parse(localStorage.getItem(SURVEY_DETAILS) || "{}");
  return payload;
}

// Get and store x-mb-token in local storage
export function setAccessToken() {
  let accessToken = getParameterByName("x-mb-token");
  localStorage.setItem(ACCESS_TOKEN_KEY, accessToken);
}

// Get and store id_token in local storage
export function setIdToken() {
  let idToken = getParameterByName("id_token");
  localStorage.setItem(ID_TOKEN_KEY, idToken);
}

export function isLoggedIn() {
  var payload = JSON.parse(localStorage.getItem(PROFILE_PAYLOAD) || "{}");
  return payload.id || false;
}

function getTokenExpirationDate(encodedToken) {
  const token = decode(encodedToken);
  if (!token.exp) {
    return null;
  }

  const date = new Date(0);

  date.setUTCSeconds(token.exp);
  isTokenExpired(token)

  return date;
}

function isTokenExpired(token) {
  const expirationDate = getTokenExpirationDate(token);
  return expirationDate < new Date();
}

export function toggleLoader() {
  return {
    type: TOGGLE_LOADER
  };
}
